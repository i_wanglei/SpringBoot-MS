var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var path = {
    scss        : ['./css/scss/*.scss'],
    frameScss   : ['./lib/tab-frame/*.scss'],
    frameJs     : ['./lib/tab-frame/*.js']
};

gulp.task('sass', function( done ){
    gulp.src(path.scss)
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest( './css/' ))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe( rename({ extname : '.min.css' }))
        .pipe( gulp.dest( './css/'))
        .on('end', done);
});

gulp.task('frameSass', function( done ){
    gulp.src(path.frameScss)
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest( './lib/tab-frame/' ))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe( rename({ extname : '.min.css' }))
        .pipe( gulp.dest( './lib/tab-frame/dist/'))
        .on('end', done);
});

gulp.task('frameJs',function(){
    gulp.src(path.frameJs)
        .pipe( uglify() )
        .pipe( rename({ extname: '.min.js' }) )
        .pipe( gulp.dest('./lib/tab-frame/dist/') )
})

gulp.task('watch', function(){
    gulp.watch(path.scss, ['sass']);
})