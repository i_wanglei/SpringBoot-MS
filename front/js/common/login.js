
$(function(){
    var docHeight = document.documentElement.clientHeight;
    $("#wrap").height(docHeight);
    $(window).resize(function(){
        var docHeight = document.documentElement.clientHeight;
        $("#wrap").height(docHeight);
    });

    $("#top-mask").height(docHeight);

});
//回车键的键值为13
$(document).keydown(function(e){
	   if (e.keyCode==13){   //回车键的键值为13
		   validateLogin();
	   }
 });
//验证用户名密码不能为空
function validateLogin(){
    var account = $("input[name='loginName']").val();
    var password = $("input[name='loginPassword']").val();
    var user = {"account":account,"password":password};
   var loginData ;
    var aj = $.ajax( {  
        url:'/service/admin/doLogin',// 跳转到 action
        data:user,  
        type:'post',  
        async:false,
        cache:false,  
        dataType:'json',  
        success:function(data) {  
        	loginData = data;
         },  
         error : function() {  
        	  showError( '用户名密码不能为空' );
        	 return false; 
         }  
    });
    if(loginData){
    	if(loginData.code=="200"){
            window.location.href='index.html';//正确登录后页面跳转至
              return true; 
    		  
    	} else{
    		
    		 showError(loginData.message);
            return false; 
    	}
    }
 
}

function detectCaps(event){
    var e = event||window.event;
    var keyCode  =  e.keyCode||e.which;
    var isShift  =  e.shiftKey ||(keyCode  ==   16 ) || false ; // shift键是否按住  
    if (
        ((keyCode >=   65   &&  keyCode  <=   90 )  &&   !isShift) // Caps Lock 打开，且没有按住shift键
            || ((keyCode >=   97   &&  keyCode  <=   122 )  &&  isShift)// Caps Lock 打开，且按住shift键
        ){
        $("#pwd-caps").show().addClass("pwdCapson");
    }else{
        $("#pwd-caps").removeClass("pwdCapson").fadeOut();
    }
}

function showError(error){
    $("#content").addClass("shak-ani");

    setTimeout(function(){
        $("#content").removeClass("shak-ani");
    },1000);

    $("#error").addClass("showUp").text(error).show();
    setTimeout(function(){
        $("#error").addClass("showDown").fadeOut(200,function(){
            $(this).text("").removeClass();
        });
    },4000)
}

if( !window.requestAnimationFrame ){
    window.requestAnimationFrame = window.webkitRequestAnimationFrame || function (callback) {
        return window.setTimeout(callback, 100 / 6 );
    };
}

var focallength = 100;

function dot(x,y,z,radius,color){
    this.x = x;
    this.y = y;
    this.z = z;
    this.radius = radius;
    this.vx = x;
    this.vy = y;
    this.vz = z;
    this.color = color;
}

dot.prototype.Draw = function(ctx){
    var scale = focallength / ( focallength + this.z );
    var width = document.documentElement.clientWidth,
        height =document.documentElement.clientHeight;
    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = this.color;
    var rds = this.radius * scale;
    if( rds > 300) rds = 50;
    if( rds < 1 ) return;
    ctx.arc(width / 5 + (this.x - width / 2) * scale,height / 5 + (this.y - height / 2) * scale,rds,0,2 * Math.PI,true);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}

//Array.prototype.forEach = function(callback){
//    for( var i = 0; i < this.length; i++){
//        callback.call(this[i]);
//    }
//}

function initBall(){
    var docWidth = document.documentElement.clientWidth,
        docHeight = document.documentElement.clientHeight,
        ballNum = 100,
        dots = [];

    var canvas = document.getElementById("draw-ball");

    $(window).resize(function(){
        var docWidth = document.documentElement.clientWidth,
            docHeight = document.documentElement.clientHeight;
        canvas.setAttribute('width',docWidth);
        canvas.setAttribute('height',docHeight);
    })
    canvas.setAttribute('width',docWidth);
    canvas.setAttribute('height',docHeight);

    var ctx = canvas.getContext("2d");

    for( var i = 0 ; i< ballNum; i++ ){

        var x = GetRandomNum(0,canvas.width * 1.5),
            y  = GetRandomNum(0,canvas.height * 1.5),
            z  = GetRandomNum( -focallength, focallength),
            radius = GetRandomNum(7,15),
            opaCity = GetRandomNum(0.2,0.75),
            bg = ['rgba(199,230,232,'+ opaCity +')','rgba(201,219,238,'+ opaCity +')','rgba(189,216,239,'+ opaCity +')','rgba(205,231,236,'+ opaCity +')'];

        var random = bg[GetRandomNum(0,3)];

        var ball = new dot(x,y,z,radius,random);
        dots.push(ball);
    }
    dots.sort(function(a, b) {
        return b.z - a.z;
    });

    var outsideBall = [];

    function move(ball){

        if( ( (ball.x - ball.vx) <= 0 && ball.x < canvas.width - 100 ) && ( (ball.y - ball.vy) <= 0 && ball.y < canvas.height - 100 ) ){
            ball.x += GetRandomNum(0.1,0.5);
            ball.vx = ball.x;
            ball.y += GetRandomNum(0.1,0.3);
            ball.vy = ball.y;
        } else {
            ball.x -= GetRandomNum(0.1,0.5);
            ball.vx = 0;
            ball.y -= GetRandomNum(0.1,0.3);
            ball.vy = 0;
        }

    }


    function draw(dot){
        dot.Draw(ctx);
    }

    (function drawFrame(){
        window.requestAnimationFrame(drawFrame, canvas);
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        for(var i = 0 ; i < dots.length;i++){
            move(dots[i]);
            draw(dots[i]);
        }
    }())


//    $("#draw-ball").on({
//       mousemove : function(event){
//            var x = event.pageX,
//                y = event.pageY,
//                width = canvas.width,
//                height = canvas.height;
//
//            var distanceX = (x - width / 2) * 0.01;
//            var distanceY = (y - height / 2) * 0.01;
//            for(var i = 0 ; i < dots.length;i++){
//                dots[i].x -= distanceX;
//                dots[i].y -= distanceY;
//                draw(dots[i]);
//            }
//        },
//      mouseout : function(event){
//          firstMoveY = firstMoveX = 0;
//      }
//    });
//    $("#content").on({
//        mousemove : function(event){
//            var x = event.pageX,
//                y = event.pageY,
//                width = canvas.width,
//                height = canvas.height;
//            for(var i = 0 ; i < dots.length;i++){
//                dots[i].z -= 0.2;
//                draw(dots[i]);
//            }
//        }
//    })

}


var browser=navigator.appName 
var b_version=navigator.appVersion 
var version=b_version.split(";"); 
var trim_Version=version[1].replace(/[ ]/g,""); 


var isIE8 =  (browser=="Microsoft Internet Explorer" )&& (trim_Version=="MSIE8.0");
  if(  isIE8==false) {
	  $(initBall);
  }



/* 此方法用来辅助上面的删除方法，提供一套随机数字*/
function GetRandomNum(Min,Max){
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
}

$(function(){
    setTimeout(function(){
        $("#content").fadeIn();
    },300);
})
