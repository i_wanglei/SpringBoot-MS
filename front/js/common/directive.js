(function () {
    "use strict";
    /**
     * CPS Directives
     * Author Vicco Wang
     * 2016-07-20
     */
    var commonDirective = angular.module("commonDirectives", []);

    /**
     *
     * 页面顶部菜单栏
     *
     */
    commonDirective.directive('zhxToolbar', ['$compile','toolbarService', function ($compile,toolbarService) {
        return {
            restrict: 'E',
            scope: true,   //Toolbar中按钮的方法都在外层控制器中定义，所以Toolbar不能使用完全独立的作用域，否则无法获取对应方法，使按钮失效。
            replace: true,
            template:   "<div class='toolbar'>" +
            "<ul class='nav-toolbar'>" +
            "<li ng-repeat='bar in toolbarData'>" +
            "<a ng-if='!bar.child.length' class='toolbar-btn toolbar-btn-default' ng-click='$eval(bar.resourceUrl)'>" +
	"<div  style='width:14px;height:14px; margin-right:5px; background:url(\"images/{{ bar.resourceImage }}\");background-repeat: no-repeat;'> </div>"+
                //"<span class='zhx-icon-font {{ bar.navigationImage }}'></span>" +
            "{{ bar.resourceName }}" +
            "</a>" +
            "<a ng-if='bar.child.length' class='toolbar-btn hasChild toolbar-btn-default' ng-click='showChildMenu($event,bar.child)'>" +
            "{{ bar.name }}" +
            "<i class='zhx-icon-font icon-down'></i>" +
            "</a>" +
            "</li>" +
            "</ul>" +
            "</div>",
            link: function (scope, element, attrs) {

                scope.toolbarName = attrs.toolbarName;

                scope.toolbarUrl = attrs.toolbarUrl;


                //从框架继承来的获取当前tab标签页的方法
                var tabId = scope.getTabId();

                /**
                 * 根据权限获取当前菜单数据
                 */
                toolbarService.getToolbar(scope.toolbarName,scope.toolbarUrl)
                    .then(function (data) {
                        scope.toolbarData = data.data;
                    });

                /**
                 * 如果该菜单下含有子菜单，则生成一个下拉菜单
                 * @param ev        当前点击的对象
                 * @param child     当前菜单下的子菜单数组对象
                 */
                scope.showChildMenu = function(ev,child){

                    ev.stopPropagation();

                    angular.element('.nav-toolbar-childMenu').remove();

                    var eventTarget = angular.element(ev.target);

                    if(  angular.element(ev.target)[0].nodeName !== "A" ){
                        eventTarget = eventTarget.parent();
                    };

                    var childMenuScope = scope.$new();

                    childMenuScope.child = child;

                    childMenuScope.pos = {
                        left: eventTarget[0].offsetLeft,
                        top : eventTarget.outerHeight() + 1
                    };

                    var tpl = $compile( "<div class='nav-toolbar-childMenu' ng-style='{ left: pos.left, top : pos.top }'>" +
                    "<ul>" +
                    "<li ng-repeat='c in child' ng-click='$eval(c.url)'>{{ c.name }}</li>" +
                    "</ul>" +
                    "</div>")(childMenuScope);

                    childMenuScope.removeTpl = function(){
                        tpl.remove();
                        childMenuScope.$destroy();
                        angular.element('body').off('click',childMenuScope.removeTpl);
                    };

                    angular.element('body').on("click",childMenuScope.removeTpl);

                    tpl.appendTo( angular.element('.' + tabId) );

                }

            }
        }
    }])



})();







