(function () {
    "use strict";
    /**
     * CPS Services
     * Author Vicco Wang
     * 2016-07-20
     */
    var commonService = angular.module('commonServices', ['ngNotify']);

    /**
     * 获取页面Toolbar相关服务
     */
    commonService.factory("toolbarService", ['$http', '$q', function ($http, $q) {

        var service = {};

        service.getToolbar = function (toolbarName) {
          var language =  localStorage.getItem("language");
            if (language == undefined) {
                language = "CHINE_NEW";
            }
            var deferred = $q.defer();
            $http.get('/service/SysResource/getButtonResouce/' + toolbarName+"/"+language)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function(error){
                    deferred.reject(error);
                })
            return deferred.promise;

        };

        return service;

    }])

    /**
     * 公共弹出通栏提示
     * @message  消息
     * @type  消息类型
     * info 蓝色
     * error 红色
     * success 绿色
     * warn 黄色
     */
    commonService.factory("notifyService", ['ngNotify', function (ngNotify) {

        var service = {};
        var config = {
            theme: 'zhx_theme',
            position: 'bottom',
            duration: 1500,
            type: 'info',
            sticky: false,
            html: false
        }
        ngNotify.config(config);

        ngNotify.addTheme('zhx_theme', 'zhx_notify');

        service.notify = function (message, type) {
            ngNotify.set(message, {
                type: type ? type : 'info'
            });
        }
        service.showMessage = function(data){
            if(data){

                if(data.code=="500"){
                    ngNotify.set(data.message, {type: 'warn'});

                }
                if(data.code=="200"){
                    ngNotify.set(data.message, {type: 'success'});
                }
            }
        }

        return service;
    }]);


    /**
     * 标准提交操作
     */
    commonService.factory("submitService", ['$http', '$q', function ($http, $q) {

        var service = {};

        service.submit = function (url, data) {
           url = "/service/"+url 
           var method = 'POST';
           if ( data == undefined) {
                var method = 'GET';
           }
            var data = data || {};
             
            var deferred = $q.defer();
            $http({
                method: method,
                url: url,
                data: data  // pass in data as strings
                //headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            }).success(function (data) {

                deferred.resolve(data);

            }).error(function (reason) {

                deferred.reject(reason);

            });

            return deferred.promise;

        };



        

        return service;
    }]);


    /**
     * 翻页Service
     */
    //commonService.factory("paginationService", ['$http', '$q', function ($http, $q) {
    //    var service = {};
    //
    //    var pageData = {};
    //
    //    service.setPageData = function (data) {
    //        pageData = data;
    //    };
    //
    //    service.getPageData = function () {
    //        return pageData;
    //    };
    //
    //    service.getPage = function (url, pageData) {
    //
    //        console.log($.param(pageData));
    //
    //        var deferred = $q.defer();
    //        $http({
    //            method: 'POST',
    //            url: url,
    //            data: $.param(pageData),  // pass in data as strings
    //            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
    //        }).success(function (result) {
    //
    //            deferred.resolve(result);
    //
    //        }).error(function () {
    //
    //            deferred.reject("There is a error.");
    //        });
    //
    //        return deferred.promise;
    //
    //    }
    //
    //    return service;
    //
    //}]);


    commonService.factory("searchService", function () {
        var service = {};

        var searchData = [];

        service.setSearchData = function (data) {
            searchData = data;
        };

        service.getSearchData = function () {
            return searchData;
        };

        return service;

    })


    /**
     * 模态框再次封装成一个Service,以便各种情况的复用。
     */
    commonService.factory("modalService", ["$modal", function ($modal) {
        var service = {}

        var modalDefault = {
            backdrop: 'static',
            //size : 'sm',
            templateUrl: BASE_URL + '/views/components/window.html'
        }

        var modalOptions = {
            headerText: '标题',
            bodyText: '内容',
            buttonHide: false
        };

        service.show = function (customModalDefault, customModalOptions) {
            if (!customModalDefault) customModalDefault = {};
            customModalDefault.backdrop = 'static';
            return service.showModal(customModalDefault, customModalOptions);
        };

        service.showModal = function (customModalDefault, customModalOptions) {

            var tempModalDefault = {},
                tempModalOptions = {};

            angular.extend(tempModalDefault, modalDefault, customModalDefault);

            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefault.controller) {

                tempModalDefault.controller = function ($scope, $modalInstance) {

                    $scope.modalOptions = tempModalOptions;

                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };

                    $scope.modalOptions.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }

                }
            }

            return $modal.open(tempModalDefault).result;

        }

        return service;
    }]);

    commonService.factory("getZtreeService", function () {
        var service = {};


        return service;

    })


//页面跳转传参-避免使用URL不友好的方式传参
    commonService.factory('pageRedirectionService', function () {
        var savedData = {}

        function set(data) {
            savedData = data;
        }

        function get() {
            return savedData;
        }

        return {
            set: set,
            get: get
        }

    });

})();




