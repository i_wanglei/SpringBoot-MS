(function(){
    'use strict';

    var app = angular.module('zhxProject');
    /**
     * CPS Controller
     * Author Vicco Wang
     * 2016-08-09
     */

    /**
     * 项目框架总控制器
     */
    app.controller('zhxFrameController',['$rootScope','$scope','ngDialog','zhxAlertService','$http',function($rootScope,$scope,ngDialog,zhxAlertService,$http){


      //语言设置

      $scope.languageList = ["CHINE_NEW", "CHINE_OLD", "English", "FRENCH", "DUTCH", "GERMAN", "GREEK", "HUNGARIAN", "ITALIAN", "PORTUGUESE", "SPANISH", "TURKISH", "POLISH", "CZECH", "MALAY", "INDONESIAN", "SLOVAK", "ROMANIAN", "SLOVENIAN", "THAI", "SERBIAN", "GALICIAN", "VIETNAMESE", "BRAZILIAN", "JAPANESE", "LATINESP", "FARSI", "CROATIAN", "RUSSIAN", "ARABIC", "CATALAN", "DANISH", "FINNISH", "FRENCH_CA", "SWEDISH", "EUSKERA", "ALBANIAN", "BENGALI", "BULGARIAN", "CAMBODIAN", "ESTONIAN", "HEBREW", "KOREAN", "LAOTIAN", "LATVIAN", "LITHUANIAN", "MACEDONIAN", "MYANMAR", "UKRAINIAN"];
       var language =  localStorage.getItem("language");
        if (language == undefined) {
            $scope.language = "CHINE_NEW";
        }else{
           $scope.language=language;
        }
      

      $scope.selectChange = function (e) {
        //console.log(e.language)
        window.localStorage.setItem("language",e.language);
        window.location.href='index.html';//正确登录后页面跳转至
      }
     //根据系统语言,从服务器获取对应的资源
      $http.get('/service/SysI18n/get/'+language).then(function(res){
            if( res.data.code == "200" ){
                console.log(res.data.data);
                $rootScope.i18n = res.data.data;
                //有的模块不在scope范围内,需要将数据给放置localStorage
                window.localStorage.setItem("i18n",JSON.stringify(res.data.data));
                
            }else{
                console.log("get system i18n error");
            }
        })



        /**
         * 获取当前用户信息
         */
        $http.get('/service/admin/getCurrentUser').then(function(res){
            if( res.data.code == "200" ){
                $rootScope.currentUser = res.data.data;
                $scope.userName = $rootScope.currentUser.userAccount;
            }else{
                window.location.href='login.html';//正确登录后页面跳转至
            }
        })

        /**
         * 修改密码事件
         */
        $scope.changePwd = function(){

            ngDialog.open({
                templateUrl : 'tpl/changePassword.html',
                scope : $scope,
                controller : 'changePassword',
                size : 400
            });

        };

        /**
         * 用户注销登录事件
         */
        $scope.logout = function(){
        
            $scope.modalOptions = {
                headerText : '注销 '+ $scope.userName ,
                bodyText   : '确认要注销并返回登录页面吗?'
            }
            ngDialog.open({
                templateUrl : 'tpl/window.html',
                size : 350,
                scope : $scope
            });
               $scope.ok = function () {
                  $http.post("/service/admin/logout");
                  window.location.href='login.html';//正确登录后页面跳转至
                 ngDialog.close();
            }

        };

     

        //这里是弹出警报提醒的例子
        $scope.openAlert = function(level){
        
           switch( level ){
               case 'normal':
                   zhxAlertService.open({
                       message : "测试一条普通的提醒"
                   });
                   break;
               case 'warn':
                   zhxAlertService.open({
                       message : "测试一条重要的提醒",
                       level : 'warn'
                   });
                   break;
               case 'danger':
                   zhxAlertService.open({
                       message : "测试一条非常重要的提醒，且已固定顶部,无法关闭",
                       level   : "danger",
                       processEvent : function( alert ){
                           console.log("do sth after click process button.");
                           //close Alert at last;
                           alert.closeAlert(true);
                       },
                       detailEvent : function( alert ){
                           console.log("check the detail of this alert.");
        
                           //close Alert at last;
                           alert.closeAlert(true);
                       }
                   });
           }
        };
    //  $scope.openAlert("danger");
    }]);

    //修改密码弹出框控制器
    app.controller("changePassword",["$scope","$timeout","$rootScope","$http",function($scope, $timeout,$rootScope,$http){

            $scope.userData = {
                userAccount : $rootScope.currentUser.userAccount,
                userPassword : ''
            };

            //修改密码
            $scope.confirm = function(){
                  var userData = {
                          userAccount : $rootScope.currentUser.userAccount,
                          userPassword : $scope.userData.newPassword
                    };
                   $http.post("/service/SysUser/editPwd",userData).success(function(data){
                       if(data){
                          if( data.code=="200" ){
                               $scope.closeThisDialog();                                                     
                                //密码修改成功后需要清除当前COOKIE，并重新登录
                                 // ngNotify.set('密码修成功', {type: 'info'});
                                 $http.post("/service/admin/logout")
                                 window.location.href='login.html';//正确登录后页面跳转至
                            } else {
                                alert("fail");
                                // ngNotify.set('密码修改失败,请联系管理员', {type: 'error'});
                            }
                       }
                 });
                }

  
    }]);

})()
