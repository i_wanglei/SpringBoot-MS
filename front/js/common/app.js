(function(){
    'use strict';

    var app = angular.module('zhxProject',[
        'oc.lazyLoad',
        'ngSanitize',
        'zhxTabFrame',
        'zhxValidate',
        'zhxNextPage',
        'zhxAlert',
        'zhxDataGrid',
        'ngDialog',
        'ngNotify',
        'tm.pagination',
        'commonDirectives',
        'commonServices'
    ]);
     var language =  localStorage.getItem("language");
    if (language == undefined) {
        language = "CHINE_NEW";
    }
    app.config(['$ocLazyLoadProvider','zhxFrameProvider','ngDialogProvider',function($ocLazyLoadProvider, zhxFrameProvider,ngDialogProvider){

        //需要配置框架基础属性
        zhxFrameProvider.config({
            //设置为true则会将获取到的菜单信息打在控制台
            debug : true,
            //数据读取的路径
            dataUrl  : "/service/SysResource/getUserResouse/"+language,
            //项目的LOGO名称
            logoText : "SpringBootManagerSystem",
            /**
             * 数据获取后的格式路径，默认data, 数据在组件中通过$http获取后，在返回对象中
             * 会获取数据 result.data ，这里的data为数据获取路径，如果项目数据获取路径有变化
             * 例如数据最终获取为 data.res,那这里可以定义
             *  dataSrc : "data.res"
             *  即可。组件会拼接为result.data.res来获取最终的数据对象
             */
            dataSrc : "data.data[0].child",
            /**
             * 获取JSON对象的数据格式的KEY名称定义，默认id 为不可变项目，其他都可以自由配置
             * 可以配置以下KEY（以下为组件默认的KEY，根据自己项目进行修改）
             * order,name,icon,moduleName,template,children
             * @type {Object}
             */
            dataFormat : {
                "moduleName"    : "module",
                "template"      : "url",
                "children"      : "child"
            },
            //对应框架的总控制器 可以为字符串或者 标准控制器对象，也可以是简写的控制器方法
            controller : 'zhxFrameController',
            // LOGO如果是图片，这里设置URL路径
            // logoUrl : "",
            //头部高度
            headerHeight : 40,
            //左侧导航宽度
            navigationWidth : 150,
            //右侧隐藏功能菜单宽度
            settingWidth : 120
            //框架暂时未支持底部定义
            // footerHeight : 30,
            // isShowFooter : false
        });

        /**
         * 定义全局弹出框基础配置项
         */
        ngDialogProvider.setDefaults({
            closeByDocument: false  //点击屏幕遮罩不能关闭弹出框
        });

        /**
         * 模块加载配置项
         */
        $ocLazyLoadProvider.config({

            debug : true,

            /**
             * 定义导航模块以及每个模块需要的控制器
             * 静态文件，新增移除模块，这里必须预先定义好
             *
             * @name    [String] 模块名称 ex: myModules
             * @files   [Array] 控制器的相对路径 ex: js/modules/someController.js
             */
            modules: [
                {
                    name: 'userInfo',
                    files: ['js/modules/auth/user/controllers.js']
                },{
                    name: 'roleInfo',
                    files: ['js/modules/auth/role/controllers.js']
                },{
                    name: 'resourceInfo',
                    files: ['js/modules/auth/resource/controllers.js']
                },{
                    name: 'logInfo',
                    files: ['js/modules/auth/log/controllers.js']
                },
                {
                    name: 'orgInfo',
                    files: ['js/modules/auth/organization/controller.js']
                },
                {
                    name: 'csdn',
                    files: ['js/modules/third/csdn/controllers.js']
                }

            ]

        });

    }]);

    //定义全局momentJS组件中文化
    if( angular.isDefined( moment ) ) moment.locale('zh-cn');

    //定义全局Layer配置项
    if( angular.isDefined( layer ) ){

        layer.config({
            path: 'lib/layer'
        });

    }

})();
