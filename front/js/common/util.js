/**
 * common js
 * Designed by vicco
 * 2015-10-09
 */
'user strict',
/**
 * JAVASCRIPT 全局数组对象新增 indexOf 和 remove 两个方法
 * 用来移除数组中指定值（非下标）
 */
Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.max=function(){
	var max = this[0];

	for(var i=1;i<this.length;i++){ 

	  if(max<this[i])max=this[i];
	  }
     return max;
} 
function cacl(arr, callback) {
	  var ret;
	  for (var i=0; i<arr.length;i++) {
	    ret = callback(arr[i], ret);
	  }
	  return ret;
	}
Array.prototype.min = function () {
	  return cacl(this, function (item, min) {
	    if (!(min < item)) {
	      return item;
	    }
	    else {
	      return min;
	    }
	  });
	};
Array.prototype.sum = function () {
	  return cacl(this, function (item, sum) {
	    if (typeof (sum) == 'undefined') {
	      return item;
	    }
	    else {
	      return sum += item;
	    }
	  });
	};
	Array.prototype.avg = function () {
	  if (this.length == 0) {
	    return 0;
	  }
	  return this.sum(this) / this.length;
	};
/**
 * Cookie 信息操作
 */

function setCookie(name,value,time){ 
    var str = name + "=" + escape(value);
    if(time > 0){
        var date = new Date();
        var ms = time*3600*1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toGMTString();
    }
    document.cookie = str;
}
//获取cookie  
function getCookie(name){  
    //cookie中的数据都是以分号加空格区分开  
    var arr = document.cookie.split("; ");  
    for(var i=0; i<arr.length; i++){  
        if(arr[i].split("=")[0] == name){  
            return arr[i].split("=")[1];  
        }
    }
    //未找到对应的cookie则返回空字符串  
    return '';  
}
//删除cookie  
function removeCookie(name){   
    document.cookie = name+"=;expires="+(new Date(0)).toGMTString();
}

/**
 * 日期格式化
 * @param fmt
 * @returns {*}
 * @constructor
 */
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


/**
 * 日期计算函数
 * @param AddDayCount
 * @returns {String}
 */
function getDateStr(AddDayCount) {
    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    return  dd.Format("yyyy-MM-dd");
}

/**
 * 时间计算函数
 * @param AddDayCount
 * @returns {String}
 */
function getDateTimeStr(AddDayCount) {
    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    return  dd.Format("yyyy-MM-dd HH:mm:ss");
}

function getDateTimeStrhh(AddDayCount) {
    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    return  dd.Format("yyyy-MM-dd hh:mm:ss");
}

/**
 * 字符串转date
 * @param str
 * @returns {*}
 */
function strToDate(str) {
    if(str){
        str = str.replace(/-/g,"/");
        var date = new Date(str);
        return date;
    }

}

function DateFormat(date, fmt) { //author: meizz 
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}