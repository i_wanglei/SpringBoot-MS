(function () {
    'use strict';
    /**
     *
     * 资源管理 控制器
     *
     */
    var resource = angular.module('zhxProject.resource', []);

    var selectedTreeNode;

    resource.controller('resourceToolbarcontroller', function ($rootScope,$scope, ngDialog, notifyService, submitService) {
       var language =  localStorage.getItem("language");
            if (language == undefined) {
                language = "CHINE_NEW";
            }
        $scope.i18n = $rootScope.i18n;

        $scope.resourceLever = [
           /* {id: '1', name: '系统级别'},*/
            {id: '2', name: $scope.i18n.resource_resourceLever_button},
            {id: '3', name: $scope.i18n.resource_resourceLever_meun}
        ];
        /**
         * 打开新增资源新按钮
         */
        $scope.addNew = function () {
            ngDialog.open({
                templateUrl: 'js/modules/auth/resource/resource-add.html',
                scope: $scope,
                controller: 'resourceAdd',
                size: 600
            });
        };
        //** 修改选中数据//
        $scope.modify = function () {
            ngDialog.open({
                templateUrl: 'js/modules/auth/resource/resource-edit.html',
                scope: $scope,
                controller: 'resourceEdit',
                size: 600
            });
        }
        $scope.removeList = function () {
            submitService.submit('SysResource/remove/' + selectedTreeNode.id).
                then(function (data) {
                	notifyService.showMessage(data);
                	$scope.initResouceNode();
                });
        }
      //** 初始化资源信息列表
        $scope.initResouceNode = function (){
        	  var resourceSetting = {
        			  check: {
        	                enable: false
        	            }, callback: {
        	                onClick: onClick
        	            },
        	            data: {
        	                simpleData: {
        	                    enable: true
        	                }
        	            }
        	         };
        	  submitService.submit("SysResource/getAll/"+language).then(function (data) {
                 var resourceTree = $.fn.zTree.init($("#resourceTreeDiv"), resourceSetting, data.data);
                  var nodes = resourceTree.getNodes();
                  for (var i = 0; i < 4; i++) { //设置节点展开
                	  resourceTree.expandNode(nodes[i], true, false, true);
                  }
              });
        	  //点击资源树节点的时候查询资源树的信息
          function onClick(event, treeId, treeNode) {
              selectedTreeNode = treeNode;
              submitService.submit("SysResource/getByPk/" + treeNode.id+"/"+language).then(function (data) {
                      $scope.resource = data.data;
                });
          }
        }
        //页面初次加载的时候初始化资源树
        $scope.initResouceNode();
        
    });

    /**
     * 资源新增数据 控制器
     * @$modalInstance  模态框引用
     * @gridService
     */
    resource.controller('resourceAdd', function ($scope, ngDialog, submitService, notifyService, $http) {
        //默认值设置
        $scope.resourceAdd = {resourceEnable: "1", resourceLever: '2'};
        $scope.ok = function () {
            $scope.resourceAdd.resourceParentUuid = selectedTreeNode.id;
            submitService.submit('SysResource/add', $scope.resourceAdd).
            then(function (data) {
            	//显示消息
            	notifyService.showMessage(data);
            	//重新加载资源树
            	$scope.initResouceNode();
            	ngDialog.close();
            });
        }
        $scope.cancel = function () {
            ngDialog.close();
        }
    });
    /**
     * 修改资源
     * @$modalInstance  模态框引用
     * @gridService
     */
    resource.controller('resourceEdit', function ($scope, ngDialog, submitService, notifyService, $http) {
        var language =  localStorage.getItem("language");
            if (language == undefined) {
                language = "CHINE_NEW";
            }
        //查询数据反现
        submitService.submit("SysResource/getByPk/"+selectedTreeNode.id+"/"+language).then(function (data) {
                $scope.resource = data.data;
            });
        $scope.ok = function () {
            submitService.submit('SysResource/edit', $scope.resource).then(function (data) {
            	//显示修改状态
            	notifyService.showMessage(data);
            	//重新加载资源树
            	$scope.initResouceNode();
            	ngDialog.close();
            });
        }
        /**
         *关闭按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }
    });



       
    

    resource.filter("status", function(){
      var i18n =  JSON.parse(localStorage.getItem("i18n"));
        return function( status ){
            switch( status ){
                case "1":
                    return i18n.status_enable;
                    break;
                case "0":
                    return i18n.status_unable;
                    break;
                default:
                    return "";
            }
        }
    });

})();

