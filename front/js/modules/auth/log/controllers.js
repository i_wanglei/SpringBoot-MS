(function () {
    'use strict';
    /**
     *
     * 日志管理 控制器
     *
     */
    var logInfo = angular.module('zhxProject.logInfo', ['datePicker']);
    logInfo.controller('logListController', function ($rootScope, $scope, ngDialog, ngNotify, gridService, submitService) {
        $scope.i18n = $rootScope.i18n;
        $scope.log = {
            startTime: getDateStr(0) + " 00:00:00",
            endTime: getDateStr(0) + ' 23:59:59',
            pageSize: 10, pageNum: 1,
            logUserAccount: "admin"
        };
        //初始化下拉框
        submitService.submit('SysUser/getSubUser/'+$scope.log.logUserAccount).
            then(function (data) {
                if (data.code == '200') {
                    $scope.userList = data.data;
                }
            });
        $scope.initTableList = function () {
            /**
             * 初次加载页面加载列表数据
             */
            submitService.submit('SysLog/getPage', $scope.log).
                then(function (data) {
                	 $scope.gridOptions.gridData = data.list;
                     $scope.gridOptions.paginationConf.totalItems = data.total;
                });
        }

        $scope.initTableList();
        $scope.gridOptions = {
                scope : $scope,
                columnDefs: [
                    { displayName: $scope.i18n.grid_rowNumber, name: "zhxRowNumber"},
                    { displayName: $scope.i18n.log_logUserAccount, name: "logUserAccount" },
                    { displayName: $scope.i18n.log_logModule, name: "logModule"},
                    { displayName: $scope.i18n.log_logFunction, name: "logFunction"},
                    { displayName: $scope.i18n.log_logOperation, name: "logOperation"},
                    { displayName: $scope.i18n.log_logDesc, name: "logDesc"},
                    { displayName: $scope.i18n.log_logIp, name: "logIp"},
                    { displayName: $scope.i18n.log_logType, name: "logType"},
                    {
                        displayName: $scope.i18n.log_logCreateTime,
                        name: "logCreateTime",
                        width:150,
                        enableSorting: true,
                        
                        /**
                         * $filter('filter')(array, expression, comparator)
                         * 过滤器具体用法请参考AngularJS API 关于 $filter 服务的部分
                         *
                         * 如果是自定义的简单过滤器，只最要传递一个字符串即可，如果是复杂过滤器带表达式等，需要传递一个对象
                         * @param name 过滤器名称
                         * @param params 过滤器需要的参数数组，对应AngularJS的文档，为 [ expression, comparator ];
                         */
                        filter :{
                            name : 'date',
                            params : ['yyyy-MM-dd HH:mm:ss']
                        }
                    }
                ],
                flexHeight : 210,
                //height : 500,
                sorter: function (order, desc) {
                    $scope.log.orderBy = order;
                    $scope.log.descOrAsc = desc;
                    $scope.initTableList();
                },
                paginationConf: {
                    currentPage: 1,
                    itemsPerPage: 10,
                    perPageOptions: [10, 20, 30, 40, 50, 100, 200, 300],
                    onChange: function () {
                    	$scope.log.pageSize = $scope.gridOptions.paginationConf.itemsPerPage;
                    	$scope.log.pageNum = $scope.gridOptions.paginationConf.currentPage;
                        $scope.initTableList();
                    }
                }

            }
         $scope.ok = function () {
            $scope.initTableList();
        };
    });
})();