(function () {
    "use strict";
    /**
     *
     * 角色管理 控制器
     *
     */
    var rolesModel = angular.module('zhxProject.Roles', []);

    rolesModel.controller('roleToolbarcontroller', function ($rootScope,$scope, ngDialog, notifyService, gridService, submitService) {
         $scope.roleParameters = {pageSize: 10, pageNum: 1};
         $scope.i18n = $rootScope.i18n;
        /**
         * 打开新增角色新按钮
         */
        $scope.addNew = function () {
            ngDialog.open({
                templateUrl: ' js/modules/auth/role/role-add.html',
                scope: $scope,
                controller: 'roleAdd',
                size: 600
            });
        };


        /**
         * 打开角色授权按钮
         */
        $scope.authorize = function () {
            var checked = $scope.getCheckedItems();
            if (!checked.length) {
                notifyService.notify($scope.i18n.message_grid_check, 'warn');
            } else {

                ngDialog.open({
                    templateUrl: ' js/modules/auth/role/role-authorize.html',
                    scope: $scope,
                    controller: 'roleAuthorize',
                    size: 400
                });
            }
        };

 

        /**
         * 修改选中数据
         */
        $scope.modify = function () {
            var checked = $scope.getCheckedItems();
            if (!checked.length || checked.length > 1) {
                notifyService.notify($scope.i18n.message_grid_check,  'warn');
            } else {
                $scope.roles = checked[0];
                ngDialog.open({
                    templateUrl: ' js/modules/auth/role/role-edit.html',
                    scope: $scope,
                    controller: 'roleEdit',
                    size: 600
                });
            }
        }
        /**
         * 删除选中数据
         */
        $scope.removeList = function () {
            var checked = $scope.getCheckedIds();
            if (!checked.length  ) {
                notifyService.notify($scope.i18n.message_grid_check, 'warn');
            } else {
                submitService.submit('SysRole/remove/' + checked).then(function(data){
                    notifyService.showMessage(data);
                    $scope.initTableList();
                });

            }
        }
        /**
         * 查询按钮使用
         */
        $scope.showSearch = function () {
            ngDialog.open({
                templateUrl: ' js/modules/auth/role/role-search.html',
                scope: $scope,
                controller: 'roleSearch',
                size: 600
            });
        };


        $scope.data = [];
        $scope.$on('roleParameters', function(event, data) {
            $scope.roleParameters = data;
        });
        $scope.initTableList = function(){
            submitService.submit('SysRole/getPage', $scope.roleParameters).
            then(function (data) {
                $scope.gridOptions.gridData = data.list;
                $scope.gridOptions.paginationConf.totalItems = data.total;
                notifyService.showMessage(data);
            });
        }
        $scope.initTableList();
        $scope.gridOptions = {
                gridData: [],
                scope : $scope,
                checkModel: true,  
                columnDefs: [

                    { displayName: $scope.i18n.grid_rowNumber, name: "zhxRowNumber", width:100 },
                    { displayName: $scope.i18n.role_rolesName, name: "rolesName", width:150, enableSorting: true},
                    { displayName: $scope.i18n.role_rolesDesc, name: "rolesRemark", enableSorting: true},
                    { displayName: $scope.i18n.role_rolesCreateUser, name: "rolesCreateUser", enableSorting: true},
                    { displayName: $scope.i18n.role_rolesEnable, name: "rolesEnable", filter: 'status', enableSorting: true},
                    {
                        displayName: $scope.i18n.role_rolesCreateTime,
                        name: "rolesCreateTime",
                        width:200,
                        /**
                         * $filter('filter')(array, expression, comparator)
                         * 过滤器具体用法请参考AngularJS API 关于 $filter 服务的部分
                         *
                         * 如果是自定义的简单过滤器，只最要传递一个字符串即可，如果是复杂过滤器带表达式等，需要传递一个对象
                         * @param name 过滤器名称
                         * @param params 过滤器需要的参数数组，对应AngularJS的文档，为 [ expression, comparator ];
                         */
                        filter :{
                            name : 'date',
                            params : ['yyyy-MM-dd HH:m:ss']
                        }
                        /**
                         * render 可以独立渲染单元格内容，接收两个参数
                         * @param value 当前单元格值
                         * @param row  行对象， 这里限于参数无法传递对象，因此这里为 JSON string,根据使用可以通过 JSON.parse()转为JSON对象
                         *
                         * 这里注释表示可以自行使用过滤器服务来实现日期的格式化
                         */
                        //renderer:function(value,row){
                        //    var strDate =  $filter('date')(value,'yyyy-MM-dd');
                        //    return "<span style='color:red'>" + strDate + "</span>";
                        //}
                    } 
                ],
                flexHeight : 180,
                //height : 500,
                sorter: function (order, desc) {
                    $scope.roleParameters.orderBy = order;
                    $scope.roleParameters.descOrAsc = desc;
                    $scope.initTableList();
                },
                paginationConf: {
                    currentPage: 1,
                    itemsPerPage: 10,
                    perPageOptions: [10, 20, 30, 40, 50, 100, 200, 300],
                    onChange: function () {
                        $scope.roleParameters.pageSize = $scope.gridOptions.paginationConf.itemsPerPage;
                        $scope.roleParameters.pageNum = $scope.gridOptions.paginationConf.currentPage;
                        $scope.initTableList();
                    }
                }

            }
    });

    /**
     * 角色新增数据 控制器
     * @$modalInstance  模态框引用
     * @gridService
     */
    rolesModel.controller('roleAdd', function ($scope, ngDialog, submitService, notifyService , $http) {
        $scope.roles = {rolesEnable: "1"};
        $scope.ok = function () {

            submitService.submit('SysRole/add', $scope.roles).then(function(data){
                ngDialog.close();
                notifyService.showMessage(data);
                $scope.initTableList();
            });
        }
        /**
         * 打开新增角色新按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }
    });
    /**
     * 角色授权
     */
    rolesModel.controller('roleAuthorize', function ($scope, ngDialog, gridService, submitService, notifyService, $http) {
        var resourceSetting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            }
        };

        var tree = {} ;
        var language =  localStorage.getItem("language");
        submitService.submit("SysResource/getRoleResouse/", {roleId: $scope.getCheckedIds()[0],language:language})
            .then(function (data) {
                 tree = $.fn.zTree.init($("#roleAuthorizeTree"), resourceSetting, data.data);
            });
        $scope.ok = function () {
            //角色IDS
            var roleList = $scope.getCheckedIds();
            var resouseList = tree.getCheckedNodes();
            var resouseArr = [];
            for (var i = 0; i < resouseList.length; i++) {
                resouseArr.push(resouseList[i].id);
            }
            var parameter = {roleList: roleList, resouseArr: resouseArr};
            submitService.submit('SysRole/authorize', parameter).then(function(data){
                ngDialog.close();
                notifyService.showMessage(data);
            });
        }
        /**
         * 打开新增角色新按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }
    });

    /**
     * 角色信息编辑
     */
    rolesModel.controller('roleEdit', function ($scope, ngDialog, submitService, notifyService, $http) {
        $scope.ok = function () {
            submitService.submit('SysRole/edit', $scope.roles).then(function(data){
                ngDialog.close();
                notifyService.showMessage(data);
                $scope.initTableList();
            });
        }
        /**
         *关闭按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }
    });



    /**
     * 角色查询数据 控制器
     * @$modalInstance  模态框引用
     * @gridService
     */
    rolesModel.controller('roleSearch', function ($scope, ngDialog, notifyService, $http) {
        $scope.ok = function () {
            $scope.roleParameters = new Object();
            if ($scope.rolesSearch != undefined) {
                $scope.roleParameters = $scope.rolesSearch;
            }
            $scope.roleParameters.pageSize = 10;
            $scope.roleParameters.pageNum = 1;
            $scope.$emit('roleParameters', $scope.roleParameters);
            $scope.initTableList();
            ngDialog.close();
        }

        /**
         * 关闭按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }

    });
    rolesModel.filter("status", function(){
        var i18n =  JSON.parse(localStorage.getItem("i18n"));
        return function( status ){
            switch( status ){
               case "1":
                    return i18n.status_enable;
                    break;
                case "0":
                    return i18n.status_unable;
                    break;
                default:
                    return "";
            }
        }
    })

})();