
(function () {
    'use strict';
    /**
     *
     * 组织机构 控制器
     *
     */
    var organization = angular.module('zhxProject.orgInfo', []);

    var selectedTreeNode;
   
    var isHasRootNode = false;
    

    organization.controller('organizationToolbarcontroller',  function ($rootScope,$scope, ngDialog, notifyService, submitService) {
        
        $scope.organizationAdd = {orgEnabled: "1"};
        $scope.i18n = $rootScope.i18n;
        /**
         * 打开新增组织按钮
         */
        $scope.addNew = function () {
        	if(isHasRootNode&&selectedTreeNode==null){
        		notifyService.notify($scope.i18n.message_grid_check, 'warn');
        		return;
        	}
        	
        	
	        	ngDialog.open({
	                templateUrl: 'js/modules/auth/organization/org-add.html',
	                scope: $scope,
	                controller: 'organizationAdd',
	                size: 400
	        	});
        }

      //** 修改选中数据//
        $scope.modify = function () {
        	if(isHasRootNode&&selectedTreeNode==null){
        		notifyService.notify($scope.i18n.message_grid_check, 'warn');
        		return;
        	}
            ngDialog.open({
                templateUrl: 'js/modules/auth/organization/org-edit.html',
                scope: $scope,
                controller: 'organizationEdit',
                size: 400
            });
        }
        //**删除选中数据**//
        $scope.removeList = function () {
        	if(isHasRootNode&&selectedTreeNode==null){
        		notifyService.notify($scope.message_grid_check, 'warn');
        		return;
        	}
        	 $scope.modalOptions = {
	 	                headerText : $scope.i18n.message_remove_title,
	 	                bodyText   : $scope.i18n.message_remove_content
	 	            }
	 	            ngDialog.open({
	 	                templateUrl : 'tpl/window.html',
	 	                size : 350,
	 	                scope : $scope
	 	            })
	 	                .closePromise
	 	                .then(function(res){
	 	                	if(res.value==true){
	 	                		submitService.submit('SysOrg/remove/' + selectedTreeNode.id).
	 	                		then(function (data) {
	 	                       	notifyService.showMessage(data);
	 	                       	$scope.initOrgNode();
	 	                       
	 	                       	ngDialog.close();
	 	                       
	 	                       	})
	 	                       
	 	                     }
	 	                    else{
	 	                    	  $scope.cancel = function () {
	 	                          ngDialog.close();
	 	                     }
	 	                    
        }
	 	                	}) 
        }
        
      
      //** 初始化结构信息列表
        $scope.initOrgNode = function (){
        	  selectedTreeNode=null;
        	  var organizationSetting = {
        			  check: {
        	                enable: false
        	            }, 
        	            callback: {
        	                onClick: onClick
        	            },
        	            data: {
        	                simpleData: {
        	                    enable: true
        	                }
        	            }
        	         };
        	  submitService.submit("SysOrg/getAll").then(function (data) {
                 var organizationTree = $.fn.zTree.init($("#organizationTreeDiv"), organizationSetting, data.data);
                  var nodes = organizationTree.getNodes();
                  
                  if(nodes.length!=0)
                  {
                	  isHasRootNode=true;
                  }
                  else{
                	  isHasRootNode=false;
                  }
                	  
                  
                  for (var i = 0; i < 4; i++) { //设置节点展开
                	  organizationTree.expandNode(nodes[i], true, false, true);
                  }
                 
              });
        	  //点击结构树节点的时候查询结构树的信息
          function onClick(event, treeId, treeNode) {
              selectedTreeNode = treeNode;
              submitService.submit("SysOrg/getByPk/" + treeNode.id).then(function (data) {
                      $scope.organization = data.data;
                });
          }
         
        }
        //页面初次加载的时候初始化结构树
        $scope.initOrgNode();
        
       
        
     
         
       
        
    });

   
    /**
     * 组织新增数据 控制器
     * @$modalInstance  模态框引用
     * @gridService
     */
    organization.controller('organizationAdd', function ($scope, ngDialog, submitService, notifyService, $http) {
        $scope.organization.orgName = "";
        
        //默认值设置
        $scope.ok = function () {
        	
        	if(isHasRootNode){
        		$scope.organizationAdd.orgParentUuid = selectedTreeNode.id;
        	}
			     
            submitService.submit('SysOrg/add', $scope.organizationAdd).
            then(function (data) {
            	//显示消息
            	notifyService.showMessage(data);
            	//重新加载结构树
            	$scope.initOrgNode();
            	
            	ngDialog.close();
            	
            });
           
        }
    
        $scope.cancel = function () {
            ngDialog.close();
        }
        
        
    });
    /**
     * 修改组织信息
     * @$modalInstance  模态框引用
     * @gridService
     */
    organization.controller('organizationEdit', function ($scope, ngDialog, submitService, notifyService, $http) {
        $scope.organization.orgName = selectedTreeNode.name ;

        $scope.ok = function () {
            submitService.submit('SysOrg/edit', $scope.organization).then(function (data) {
            	//显示修改状态
            	notifyService.showMessage(data);
            	//重新加载结构树
            	$scope.initOrgNode();
            	ngDialog.close();
            	
            });
            
        }
        /**
         *关闭按钮
         */
        $scope.cancel = function () {
            ngDialog.close();
        }
    });
    
 
     organization.filter("status", function(){
       var i18n =  JSON.parse(localStorage.getItem("i18n"));
        return function( status ){
            switch( status ){
                 case 0:
                    return i18n.status_unable;
                    break;
                case 1:
                    return i18n.status_enable;
                    break;  
                default:
                    return "";
            }
        }
    });

})();


   
