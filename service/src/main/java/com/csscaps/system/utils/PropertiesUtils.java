package com.csscaps.system.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
public class PropertiesUtils {
	 private static final Log log = LogFactory.getLog(PropertiesUtils.class);
	 private static Properties pros = null;//Properties Object  
		    /** 
		     * Loading files to Properties object  
		     */  
	     static {  
	    	//Create a Properties object  
	        pros = new Properties();
	        
	        //InputStream in = PropertiesUtils.class.getResourceAsStream(path);  
	        try {  
	        	pros.load(new FileInputStream(System.getProperty("user.dir")+"/application.properties"));
	            //pros.load(in);//Load the file Properties to flow to the object  
	        } catch (IOException e) {
	        	log.error("file reda /config.properties fail");
	            log.error("Exception",e)  ;
	        }  
	    }  
	      
	    /** 
	     * Through the attribute name to get attribute values 
	     * @param key 
	     * @return 
	     */  
	    public static String getProperty(String key){  
	        return pros.getProperty(key);//Through the key specific get attribute value  
	    }  
	    /** 
	     * 获取所有属性，返回一个map,不常用 
	     * 可以试试props.putAll(t) 
	     */  
	    public Map<String, Object> getAllProperty(){  
	        Map<String, Object> map=new HashMap<String, Object>();  
	        Enumeration<?> enu = pros.propertyNames();  
	        while (enu.hasMoreElements()) {  
	            String key = (String) enu.nextElement();  
	            String value = pros.getProperty(key);  
	            map.put(key, value);  
	        }  
	        return map;  
	    }  
	    public static void main(String[] args) {    
	    	System.out.println(PropertiesUtils.getProperty("mongdb.port33")); 
	    }   
	    
	  
}
