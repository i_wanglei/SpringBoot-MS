package com.csscaps.system.utils;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * 随机返回数据中一条数据
 * 随机数工具，单例模式
 * 
 * @author hehui
 * 
 */
public class RandomUtils {
	
	public static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";  
	
	private static Random random;

	// 双重校验锁获取一个Random单例
	public static Random getRandom() {
		if (random == null) {
			synchronized (RandomUtils.class) {
				if (random == null) {
					random = new Random();
				}
			}
		}
		return random;
	}

	/**
	 * 获得一个[0,max)之间的整数。
	 * 
	 * @param max
	 * @return
	 */
	public static int getRandomInt(int max) {
		return Math.abs(getRandom().nextInt()) % max;
	}

	/**
	 * 获得一个[0,max)之间的整数。
	 * 
	 * @param max
	 * @return
	 */
	public static long getRandomLong(long max) {
		return Math.abs(getRandom().nextInt()) % max;
	}

	/**
	 * 从list中随机取得一个元素
	 * 
	 * @param list
	 * @return
	 */
	public static <E> E getRandomElement(List<E> list) {
		return list.get(getRandomInt(list.size()));
	}

	/**
	 * 从set中随机取得一个元素
	 * 
	 * @param <E>
	 * @param <E>
	 * @param set
	 * @return
	 */
	public static <E> E getRandomElement(Set<E> set) {
		int rn = getRandomInt(set.size());
		int i = 0;
		for (E e : set) {
			if (i == rn) {
				return e;
			}
			i++;
		}
		return null;
	}

	/**
	 * 从map中随机取得一个key
	 * 
	 * @param <k>
	 * @param m
	 * @return
	 */
	public static <k, v> k getRandomKeyFromMap(Map<k, v> map) {
		int rn = getRandomInt(map.size());
		int i = 0;
		for (k key : map.keySet()) {
			if (i == rn) {
				return key;
			}
			i++;
		}
		return null;
	}

	/**
	 * 从map中随机取得一个value
	 * 
	 * @param map
	 * @return
	 */
	public static <k, v> v getRandomValueFromMap(Map<k, v> map) {
		int rn = getRandomInt(map.size());
		int i = 0;
		for (v value : map.values()) {
			if (i == rn) {
				return value;
			}
			i++;
		}
		return null;
	}

	 /** 
     * 返回一个定长的随机字符串(只包含大小写字母、数字) 
     *  
     * @param length 
     *            随机字符串长度 
     * @return 随机字符串 
     */  
    public static String generateString(int length) { 
    	
        StringBuffer sb = new StringBuffer();  
        Random random = new Random();  
        for (int i = 0; i < length; i++) {  
            sb.append(ALLCHAR.charAt(random.nextInt(ALLCHAR.length())));  
        }  
        return sb.toString();  
    } 
	
	public static void main(String[] args) {
		System.out.println(generateString(16));
	}
}