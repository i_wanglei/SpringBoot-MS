package com.csscaps.system.utils;

import java.security.MessageDigest;

/**
 * md5带盐的加密算法类.
 *
 */
public class MD5Encrypter {

    /**
     * md5加密算法
     * 
     * @param str 待加密字符串
     * @return 加密后的字符串
     */
    public static String md5Encrypt(String data, String salt) {
    	
        StringBuffer md5Value = new StringBuffer();
        MessageDigest md5 = null;
        
        if (StringUtils.isEmpty(data)) {
            return "";
        } else {
            try {
            	md5 = MessageDigest.getInstance("MD5");
            	data = data + salt;
                byte[] md5Bytes = md5.digest(data.getBytes("utf-8"));
                
                for (int i = 0; i < md5Bytes.length; i++) {
                    int val = ((int) md5Bytes[i]) & 0xff;
                    if (val < 16){
                        md5Value.append("0");
                    }
                    md5Value.append(Integer.toHexString(val));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return md5Value.toString();
        }
    }
    
    public static void main(String[] args) {
    	
    	System.out.println(MD5Encrypter.md5Encrypt("1002", "admin"));
    	
    }
}
