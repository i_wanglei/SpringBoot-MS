package com.csscaps.system.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 计时器
 * 秒表，记录耗时
 * @author qilei
 * 
 * @date 2016年7月6日
 */
public class TimerUtils {

	private long startTime; // 开始时间
	private long endTime; // 结束时间
	private List<Long> times = new ArrayList<Long>(); // 次数

	public TimerUtils() {
		startTime = System.currentTimeMillis(); // 排序前取得当前时间
		times.add(startTime);
	}

	/**
	 * 获取 使用的时间，可以连续适用多次
	 */
	public String getLostTimes() {
		long _startTime = startTime;
		if (times.size() > 1) {
			_startTime = times.get(times.size() - 1);
		}
		long _endTime = System.currentTimeMillis();
		times.add(_endTime);

		return dateDiff(_startTime, _endTime);
	}

	/**
	 * 计算 开始计时到结束的时间
	 * 
	 * @return
	 */
	public String getTotalLostTimes() {
		this.endTime = System.currentTimeMillis();
		times.add(endTime);

		return dateDiff(startTime, endTime);
	}

	public String dateDiff(long startTime, long endTime) {
		long diff = 0;
		// 按照传入的格式生成一个simpledateformate对象
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long ns = 1000;// 一秒钟的毫秒数long diff;try {
		// 获得两个时间的毫秒时间差异
		diff = endTime - startTime;

		long day = diff / nd;// 计算差多少天
		long hour = diff % nd / nh;// 计算差多少小时
		long min = diff % nd % nh / nm;// 计算差多少分钟
		long sec = diff % nd % nh % nm / ns;// 计算差多少秒//输出结果
		long msc = diff % nd % nh % nm % ns % 1000;// 计算差多少毫秒//输出结果
		// System.out.println(day + "天" + hour + "小时" + min + "分钟" + sec + "秒" + msc + "毫秒。");
		return day + "天 " + hour + "小时 " + min + "分钟 " + sec + "秒 " + msc + "毫秒";
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {

		TimerUtils lt = new TimerUtils();

		for (int i = 0; i < 1; i++) {
			try {
				Thread.currentThread().sleep(3160);

				System.out.println(lt.getLostTimes());;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(lt.getTotalLostTimes());
	}
}
