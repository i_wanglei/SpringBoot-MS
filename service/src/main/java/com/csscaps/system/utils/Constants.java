package com.csscaps.system.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * 常量类
 * 
 * @author alexwang
 *
 */
public class Constants {

	public static final DateFormat DATEFORMAT_Y_M_D = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat DATEFORMAT_Y_M_D_H_M_S = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	//用户注册审核状态标识
    public static final int APPROVE_IN = 0;//待审核
    public static final int APPROVE_PASS = 1;//审核通过
    public static final int APPROVE_NO_PASS = 2;//审核不通过
    
	// 可用状态标识
	public static final int DISABLED = 0;// 不可用
	public static final int ENABLED = 1;// 可用

	// 删除状态标识
	public static final String NOT_DELETED = "0";// 未删除
	public static final String DELETED = "1";// 已删除

	// AES加密算法秘钥
	public static final String AES_KEY = "CSSCA_LBS_AESKEY";

}
