package com.csscaps.system.shiro;

import java.io.Serializable;

import com.csscaps.auth.model.SysUser;

/**
 * @description：自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息
 * @author Administrator
 *
 */
public class ShiroUser implements Serializable {

    private static final long serialVersionUID = -1373760761780840081L;
    public String userUuid;
    public String userAccount;
    public String userRealName;
    public SysUser user;
   
    
	public ShiroUser(String userUuid, String userAccount, String userRealName,
			SysUser user) {
		super();
		this.userUuid = userUuid;
		this.userAccount = userAccount;
		this.userRealName = userRealName;
		this.user = user;
		 
	}
	public String getUserUuid() {
		return userUuid;
	}
	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserRealName() {
		return userRealName;
	}
	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}
	public SysUser getUser() {
		return user;
	}
	public void setUser(SysUser user) {
		this.user = user;
	}
	 

  
}