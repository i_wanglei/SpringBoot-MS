package com.csscaps.system.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.csscaps.auth.model.SysUser;



public class UserUtil {
	/**
	 * 获取当前用户对象shiro
	 * @return shirouser
	 */
	public static ShiroUser getCurrentShiroUser(){
		ShiroUser user=(ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}
	
	/**
	 * 获取当前用户session
	 * @return session
	 */
	public static Session getSession(){
		Session session =SecurityUtils.getSubject().getSession();
		return session;
	}
	
	/**
	 * 获取当前用户httpsession
	 * @return httpsession
	 */
	public static Session getHttpSession(){
		Session session =SecurityUtils.getSubject().getSession();
		return session;
	}
	
	/**
	 * 获取当前用户对象
	 * @return user
	 */
	public static SysUser getCurrentUser(){
		Session session =SecurityUtils.getSubject().getSession();
		if(null!=session){
			SysUser user = (SysUser) session.getAttribute("user");
			if(user == null){
				ShiroUser suser = getCurrentShiroUser();
				user = suser.getUser();
			}
			
			return user;
		}else{
			return null;
		}
		
	}
}
 