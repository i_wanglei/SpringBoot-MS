package com.csscaps.system.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.csscaps.system.mybatis.dao.MybatisDaoUtil;

/**
 * 返回 redisCache 和dao
 * 
 * @author Administrator
 * 
 * @param <T>
 */
public abstract class BaseDao<T> {
/*
	@Autowired
	protected RedisCache redisCache;*/

	@Autowired
	protected MybatisDaoUtil<T> dao;
	
}
