package com.csscaps.system.base;

import java.io.Serializable;

public class BaseTreeNode implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = -7959637470919292378L;
	/**
	 * 时间原因，现实现简单的
	 */
	private String id;// 树节点id
	private String pId;// 父亲节点id
	private String name;// 树节点name
	private String code;// 树节点code
	private String type;// 树节点type
	private String pname;// 父亲节点name
	boolean checked; // 节点是否被选中
	boolean open;// 节点是否打开

	private String area; //区域
	
	public BaseTreeNode() {
		super();
	}

	public BaseTreeNode(String id, String name, String type, String pname, String pId) {
		super();
		this.id = id;
		this.pId = pId;
		this.type = type;
		this.pname = pname;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

//    public boolean equals(Object obj){
//    	if(obj instanceof BaseTreeNode){
//    		BaseTreeNode node = (BaseTreeNode)obj;
//    		return (id.equals(node.id));
//    	}
//    	return super.equals(obj);
//    }
//    
//    public int hashCode(){
//    	return id.hashCode();
//    }
}
