package com.csscaps.system.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuTreeNode {

	private String id;
	private String parentId; //父级id
	private String name; //名称
	private String url; //链接
	private String icon; //图标
	private int order; //排序
	private int level=-1; //深度
	private boolean open = false; //是否打开
	private boolean hasChild = false; //有子节点吗
	private List<MenuTreeNode> items; //子节点集合
	
	
	public MenuTreeNode(){}
	public MenuTreeNode(String id, String parentId, String name, String url, String icon) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.url = url;
		this.icon = icon;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public void setItems(List<MenuTreeNode> items) {
		this.items = items;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	
	public boolean getHasChild() {
		return hasChild;
	}
	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}
	public List<MenuTreeNode> getItems() {
		return items;
	}
	
	//添加子节点
	private void addChild(final MenuTreeNode node){
		if(items == null){
			items = new ArrayList<MenuTreeNode>();
		}
	    items.add(node);
	}
	
	//递归找level
	private static int resolveLevel(final MenuTreeNode node, final Map<String, MenuTreeNode> nodes){
	    int level = node.level;
	    if(level == -2){
	        throw new RuntimeException("Node循环了, id=" + node.id);
	    }
	    if(level == -1){
	        node.level  = -2;
	        level = node.level = resolveLevel(nodes.get(node.getParentId()),nodes) +1;
	    }else{
	    	node.hasChild = true;
	    }
	    return level;
	}

	/**
	 * 构造无限级树结构
	* @param list 初始的list
	* @return
	* @throws Exception parentNode is null
	 */
	public static List<MenuTreeNode> baseTreeNode(List<MenuTreeNode> list){
		final Map<String, MenuTreeNode> nodes = new HashMap<String, MenuTreeNode>();

		//所有节点记录下来
		for(MenuTreeNode node : list){
		    nodes.put(node.getId(), node);
		}

		final MenuTreeNode root = new MenuTreeNode();
		root.level = 0;
		nodes.put("0", root);

		for(MenuTreeNode node : list){
		    final MenuTreeNode parent = nodes.get(node.parentId);
			if(parent == null){
				throw new RuntimeException("子节点有父级id，却没有找到此父级的对象");
			}
		    //添加子节点
		    parent.addChild(node);
		}
		
		int max = 0;
		for(MenuTreeNode node : list){
			max = Math.max(resolveLevel(node, nodes), max);
		}

		return root.items;
	}
	

}
