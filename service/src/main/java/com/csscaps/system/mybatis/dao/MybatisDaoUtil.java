package com.csscaps.system.mybatis.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 封装sqlSession对象，这里直接调用xml的名称传参即可，如果想要写sql可以使用sqlMapper类
 * @author ql
 *
 * @param <T>
 */
@Component
public class MybatisDaoUtil<T> {
	private static final Logger logger = LoggerFactory.getLogger(MybatisDaoUtil.class);
	

	private static final Map<String, SqlSessionFactory> sqlSessionFactoryMap = new HashMap<String, SqlSessionFactory>(); 

//系统全局待事务的 sqlSession 用于处理除了查询之外的 数据库操作
//private static SqlSession transactionSqlSession = null ;
//静态初始化系统资源
static {
	try {
		 //通过配置文件初始化sqlSessionFactory
		
		FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir")+"/mybatis.xml");
		
		InputStream inputStream = getInputStream(fileInputStream);
		SqlSessionFactory dbCenterSqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "masterDB");
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		inputStream.close();
		
		FileInputStream fileInputStream1 = new FileInputStream(System.getProperty("user.dir")+"/mybatis.xml");
		
		InputStream inputStream1 = getInputStream(fileInputStream1);
		dbCenterSqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream1, "slaverDB");
		sqlSessionFactoryMap.put("slaverDB", dbCenterSqlSessionFactory);
		inputStream1.close();
		/*inputStream.close();*/
		//初始化transactionSqlSession
		//setTransactionSqlSession( sqlSessionFactoryMap.get("masterDB").openSession(false));
	} catch (Exception e) {
		//e.printStackTrace();
		logger.error("dbCenterSqlSessionFactory init error :",e);
	}
}
public static InputStream getInputStream(FileInputStream fileInput) {
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	byte[] buffer = new byte[1024*4];
	int n = -1;
	InputStream inputStream = null;
	try {
		while ((n=fileInput.read(buffer)) != -1) {
			baos.write(buffer, 0, n);
			
		}
		byte[] byteArray = baos.toByteArray();
		inputStream = new ByteArrayInputStream(byteArray);
		return inputStream;
		
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	} catch (IOException e) {
		e.printStackTrace();
		return null;
	} finally {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
 
 
/**
 * 分页查询(也可直接使用PageInfo进行常规查询sql的分页操作)
 * @param statement
 * @param parametere
 * @return
 * @throws NoSuchMethodException 
 * @throws InvocationTargetException 
 * @throws IllegalAccessException 
 */
public PageInfo<T> selectPage(String statement, Object parameter)  {
	
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	int pageNum = 1;//默认pageNum
	int pageSize = 10;//默认pageSize
	try {
		if(logger.isDebugEnabled()){
		//	//logger.debug("statement" + "------>" + statement);
			// //logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
	 	try {
			Map<?, ?> p  = BeanUtils.describe(parameter);
			pageNum = Integer.parseInt(String.valueOf(p.get("pageNum")));
			pageSize = Integer.parseInt(String.valueOf(p.get("pageSize")));
		} catch (Exception e) {
            pageNum = 1;//默认pageNum
            pageSize = 10;//默认pageSize
        	logger.error("pageNum  pageSize error :",e);
		}
		 
		 PageHelper.startPage(pageNum,pageSize);
		 List<T> list = sqlSession.selectList(statement, parameter);
		 
		return new PageInfo<T>(list);
	} catch(Exception e) {
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		PageHelper.startPage(pageNum,pageSize);
		List<T> list = sqlSession.selectList(statement, parameter);
		return new PageInfo<T>(list);
	} finally {
		sqlSession.close();
	}
	
}

/**
 * 分页查询(也可直接使用PageInfo进行常规查询sql的分页操作)
 * @param statement
 * @param parameter
 * @param pageNum
 * @param pageSize
 * @return
 */
public  PageInfo<T> selectPage(String statement, Object parameter, int pageNum, int pageSize) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		PageHelper.startPage(pageNum, pageSize);
		List<T> list =  sqlSession.selectList(statement, parameter);
		return new PageInfo<T>(list);
	} catch(Exception e) {
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		PageHelper.startPage(pageNum,pageSize);
		List<T> list =  sqlSession.selectList(statement, parameter);
		return new PageInfo<T>(list);
	}finally {
		sqlSession.close();
	}

}

/**
 * Execute an insert statement.
 * @param statement  Unique identifier matching the statement to execute.
 * @return int The number of rows affected by the insert.
 */
public static int insert(String statement) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		result = sqlSession.insert(statement);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.insert(statement);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	return result;
}

/**
 * @param statement (Unique identifier matching the statement to execute)
 * @param parameter (A parameter object to pass to the statement)
 * @return (int The number of rows affected by the insert.)
 */
public int insert(String statement, Object parameter) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		result = sqlSession.insert(statement, parameter);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.insert(statement, parameter);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	return result;
}

/**
 * Execute a delete statement. The number of rows affected will be returned.
 * @param statement Unique identifier matching the statement to execute.
 * @return int The number of rows affected by the delete.
 */
public int delete(String statement) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		result = sqlSession.delete(statement);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.delete(statement);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	
	return result;
}

/**
 * @param statement (Unique identifier matching the statement to execute)
 * @param entity (A parameter object to pass to the statement)
 * @return (int The number of rows affected by the insert.)
 */
public int delete(String statement, Object parameter) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}

		result = sqlSession.delete(statement, parameter);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.delete(statement, parameter);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	return result;
}

/**
 * Execute an update statement. The number of rows affected will be
 * returned.
 * @param statement Unique identifier matching the statement to execute.
 * @return int The number of rows affected by the update.
 */
public int update(String statement) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		result = sqlSession.update(statement);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.update(statement);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	return result;
}

/**
 * @param statement (Unique identifier matching the statement to execute)
 * @param parameter (A parameter object to pass to the statement)
 * @return (int The number of rows affected by the insert.)
 */
public int update(String statement, Object parameter) {
	
	int result = 0;
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		result = sqlSession.update(statement, parameter);
	}catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		result = sqlSession.update(statement, parameter);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
	}finally {
		sqlSession.close();
	}
	return result;
}

/**
 * Retrieve a single row mapped from the statement key and parameter.
 * @param <Object> the returned object type
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @return Mapped object
 */
public <T> T selectOne(String statement, Object parameter) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		return sqlSession.selectOne(statement, parameter);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectOne(statement, parameter);
	} finally {
		sqlSession.close();
	}
	
	
}

/**
 * Retrieve a single row mapped from the statement key
 * @param <Object> the returned object type
 * @param statement
 * @return Mapped object
 */
public <T> T selectOne(String statement) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		return sqlSession.selectOne(statement);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectOne(statement);
	} finally {
		sqlSession.close();
	}
	

}

/**
 * Retrieve a single row mapped from the statement using a
 * {@code ResultHandler}.
 * @param statement Unique identifier matching the statement to use.
 * @param handler ResultHandler that will handle each retrieved row
 * @return Mapped object
 */
public void select(String statement, ResultHandler<?> handler) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		sqlSession.select(statement, handler);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		sqlSession.select(statement, handler);
	} finally {
		sqlSession.close();
	}
	
	
}

/**
 * Retrieve a single row mapped from the statement key and parameter using a
 * {@code ResultHandler}.
 * 
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @param handler ResultHandler that will handle each retrieved row
 * @return Mapped object
 */
public void select(String statement, Object parameter, ResultHandler<?> handler) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		sqlSession.select(statement, parameter, handler);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		sqlSession.select(statement, parameter, handler);
	} finally {
		sqlSession.close();
	}
	

}

/**
 * Retrieve a single row mapped from the statement key and parameter using a
 * {@code ResultHandler} and {@code RowBounds}
 * 
 * @param statement Unique identifier matching the statement to use.
 * @param rowBounds RowBound instance to limit the query results
 * @param handler ResultHandler that will handle each retrieved row
 * @return Mapped object
 */
public void select(String statement, Object parameter, RowBounds rowBounds, ResultHandler<?> handler) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		sqlSession.select(statement, parameter, rowBounds, handler);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		sqlSession.select(statement, parameter, rowBounds, handler);
	} finally {
		sqlSession.close();
	}
	
	
}

/**
 * Retrieve a list of mapped objects from the statement key and parameter.
 * 
 * @param <E> the returned list element type
 * @param statement Unique identifier matching the statement to use.
 * @return List of mapped object
 */
public  List<T> selectList(String statement) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
		}
		return sqlSession.selectList(statement);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectList(statement);
	} finally {
		sqlSession.close();
	}
	
}

/**
 * Retrieve a list of mapped objects from the statement key and parameter.
 * @param <E> the returned list element type
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @return List of mapped object
 */
public List<T> selectList(String statement, Object parameter) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		return sqlSession.selectList(statement, parameter);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectList(statement, parameter);
	} finally {
		sqlSession.close();
	}
	
	
}

/**
 * Retrieve a list of mapped objects from the statement key and parameter,
 * within the specified row bounds.
 * @param <E> the returned list element type
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @param rowBounds Bounds to limit object retrieval
 * @return List of mapped object
 */
public List<?> selectList(String statement, Object parameter, RowBounds rowBounds) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
		}
		return sqlSession.selectList(statement, parameter, rowBounds);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectList(statement, parameter, rowBounds);
	} finally {
		sqlSession.close();
	}
	
}

 
/**
 * The selectMap is a special case in that it is designed to convert a list
 * of results into a Map based on one of the properties in the resulting
 * objects. Eg. Return a of Map[Integer,Author] for
 * selectMap("selectAuthors","id")
 * @param <K> the returned Map keys type
 * @param <V> the returned Map values type
 * @param statement Unique identifier matching the statement to use.
 * @param mapKey The property to use as key for each value in the list.
 * @return Map containing key pair data.
 */
public Map<?, ?> selectMap(String statement, String mapKey) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("mapKey" + "------>" + mapKey);
		}
		return sqlSession.selectMap(statement, mapKey);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectMap(statement, mapKey);
	} finally {
		sqlSession.close();
	}
	
	

}

/**
 * The selectMap is a special case in that it is designed to convert a list
 * of results into a Map based on one of the properties in the resulting
 * objects.
 * @param <K> the returned Map keys type
 * @param <V> the returned Map values type
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @param mapKey The property to use as key for each value in the list.
 * @return Map containing key pair data.
 */
public Map<?, ?> selectMap(String statement, Object parameter, String mapKey) {
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
			//logger.debug("mapKey" + "------>" + mapKey);
		}
		return sqlSession.selectMap(statement, parameter, mapKey);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectMap(statement, parameter, mapKey);
	} finally {
		sqlSession.close();
	}
	
	
}

/**
 * The selectMap is a special case in that it is designed to convert a list
 * of results into a Map based on one of the properties in the resulting
 * objects.
 * 
 * @param <K> the returned Map keys type
 * @param <V> the returned Map values type
 * @param statement Unique identifier matching the statement to use.
 * @param parameter A parameter object to pass to the statement.
 * @param mapKey The property to use as key for each value in the list.
 * @param rowBounds Bounds to limit object retrieval
 * @return Map containing key pair data.
 */
public Map<?, ?> selectMap(String statement, Object parameter, String mapKey, RowBounds rowBounds) {
	
	SqlSession sqlSession = sqlSessionFactoryMap.get("masterDB").openSession(true);
	try {
		if(logger.isDebugEnabled()){
			//logger.debug("statement" + "------>" + statement);
			//logger.debug("parameter" + "------>"+	JSON.toJSONString(parameter));
			//logger.debug("mapKey" + "------>" + mapKey);
		}
		return sqlSession.selectMap(statement, parameter, mapKey, rowBounds);
	} catch(Exception e){
		SqlSessionFactory dbCenterSqlSessionFactory = sqlSessionFactoryMap.get("slaverDB");
		sqlSession = dbCenterSqlSessionFactory.openSession(true);
		sqlSessionFactoryMap.put("slaverDB", sqlSessionFactoryMap.get("masterDB"));
		sqlSessionFactoryMap.put("masterDB", dbCenterSqlSessionFactory);
		return sqlSession.selectMap(statement, parameter, mapKey, rowBounds);
	} finally {
		sqlSession.close();
	}

}

}
