package com.csscaps.system.constant;

import com.csscaps.system.utils.DateUtils;

/**
 * @file  SwaggerUIConstant.java
 * @author alexwang
 * @version 0.1
 * @desc	1： UI显示常量类
 * Copyright(C), 2017
 *		  cssscaps
 * History
 *   	1. Date: 2017-04-24 05:04:04
 *      	Author: alexwang
 *      	Modification: this file was created
 *   	2. ...
 */
public class SwaggerUIConstant {
    
    
    // 
    public static final String CBasicServiceTitle = "SpringBoot Manager System API管理系统—" +  "SpringBoot-MS Platform  API Manager System" ;
  
    // 
    public static final String description ="1:该API保持系统最新—" + "This is  Always keep the latest version of API "
                                             +"</br>2:本API 根据版本、模块划分—" + " The following API is classified as modules" 
                                             +"</br>3:您可以使用以下key作为对接测试(key:af2b3a8f5d1f458c8b3e)—" + " You can use the following key as a test(key:af2b3a8f5d1f458c8b3e)" ;
    public static final String termsOfServiceUrl =  "http://swagger.io/" ;
    
    public static final String contact =" Alex Wang (wangleigis@163.com)" ;
    
    public static final String version =  " 发布时间  " + DateUtils.getDateTime() + " Release time "  + DateUtils.getDateTime() ;
    
 
    
    
    
    
}
