package com.csscaps.system.constant;

import java.util.HashSet;

public class BaseConstant {
    
    //POST请求
    public static final String RequestModePost =  "POST" ;
    
    //Get请求
    public static final String RequestModeGet =  "Get" ;
    public static final String I18N_ID =  "i18n_id" ;
    //不受拦截的请求- 应用需要的资源文件
    public static final HashSet<String> unInterceptionSet = new HashSet<String>() {{  
        add("webjars");  
        add("api");  
        add("config"); 
        add("swagger"); 
        add("images");
        add("/admin/doLogin");
        add("/admin/logout"); 
    }};  
    
    
}
