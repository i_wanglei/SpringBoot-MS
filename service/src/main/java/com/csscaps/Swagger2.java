package com.csscaps;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.csscaps.system.constant.SwaggerUIConstant;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



/**
 * @file  Swagger2.java
 * @author AlexWang
 * @version 0.1
 * @desc	1：API自动生成文档
 * Copyright(C), 2017
 *		  cssscaps
 * History
 *   	1. Date: 2017-05-22 05:04:04
 *      	Author: alexwang
 *      	Modification: this file was created
 *   	2. ...
 */

@Configuration
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.csscaps"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(SwaggerUIConstant.CBasicServiceTitle)
                .description(SwaggerUIConstant.description)
                .termsOfServiceUrl(SwaggerUIConstant.termsOfServiceUrl)
                .contact(SwaggerUIConstant.contact)
                .version(SwaggerUIConstant.version)
                .license("Copyright © 2015 Beijing CSSCA Software Technology Co, Ltd.")
                .licenseUrl("http://www.ca-css.com/CSSCA_EN/Index.aspx")
                .build();
    }

}
