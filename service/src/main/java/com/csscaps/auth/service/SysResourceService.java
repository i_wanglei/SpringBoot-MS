package com.csscaps.auth.service;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.csscaps.auth.dao.SysResourceDao;
import com.csscaps.auth.dao.SysRoleResourceDao;
import com.csscaps.auth.model.SysResource;
import com.csscaps.auth.model.SysRoleResource;
import com.csscaps.system.base.BaseTreeNode;

import net.sf.json.JSONObject;
/**
 * @file  SysResourceServiceImpl.java
 * @author wanglei
 * @version 0.1
 * @todo	SysResource业务操作接口 ,基本的增删改成方法直接继承自SysResourceDao,这里不需要重写
 *        该类的方法只实现业务层的需要
 * Copyright(C),2015
 *	 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Service
public class SysResourceService extends SysResourceDao{

	@Autowired
	private SysResourceDao sysResourceDao;

	@Autowired
	private SysRoleResourceDao sysRoleResourceDao;
	@Autowired
	private SysI18nService sysI18nService;
	@Transactional
     public List<Object> getRoleResouse( String roleId,String language) throws Exception{
	         List<Object> resouceTreeNode = new ArrayList<Object>() ;
	         
	         Map map = sysI18nService.getI18nMap();
	 		List<SysResource> resoultList = new ArrayList<>(); 
	 		 
	 		
	         /*生成环境打开
	          * 
                    Subject subject = SecurityUtils.getSubject();
		  		     String userName = String.valueOf(((ShiroUser)subject.getPrincipal()).getUserAccount());
  	    	  List<Object> navigationList = navigationService.selectResouseByUserName(userName);*/
	         //查询所有资源信息
	         List<SysResource> resourceList = sysResourceDao.selectAll();
	         resourceList =  resourceI18n(resourceList, language);
			 //查询角色对应的资源信息
			  SysRoleResource roleResource =  new SysRoleResource();
		 	  roleResource.setRoleresourceRoleId(roleId);
	 	      List<SysRoleResource> list = sysRoleResourceDao.selectByVo(roleResource);
		      HashSet h = new HashSet();
		      for (SysRoleResource object : list) {
			    h.add(object.getRoleresourceResourceId());
		      }
		      //根据用户的角色资源信息，判断资源是否被选中
		    	for(int i = 0; i<resourceList.size();i++){
				SysResource resouce =   resourceList.get(i);
				BaseTreeNode treeNode = new BaseTreeNode();
				treeNode.setId(resouce.getResourceUuid());
				treeNode.setpId(resouce.getResourceParentUuid());
				treeNode.setName(resouce.getResourceName());
				if(h.contains(resouce.getResourceUuid())){
	  					treeNode.setChecked(true);
	  					treeNode.setOpen(true);
	  			}
				resouceTreeNode.add(treeNode);
			}
		 return resouceTreeNode;
	   
   }
	public List<SysResource> resourceI18n(List<SysResource> list,String language) {
		Map map = sysI18nService.getI18nMap();
		List<SysResource> resoultList = new ArrayList<>(); 
		for (SysResource sysResource : list) {
		   Map i18nMap	= (Map) map.get(sysResource.getId());
		   String name = String.valueOf(i18nMap.get(language));
		   sysResource.setResourceName(name);
		   resoultList.add(sysResource);
		}
		return resoultList;
	}
	
}
