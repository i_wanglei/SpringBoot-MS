package com.csscaps.auth.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.csscaps.auth.dao.SysRoleDao;
import com.csscaps.auth.dao.SysRoleResourceDao;
import com.csscaps.auth.dao.SysUserRoleDao;
import com.csscaps.auth.model.SysRoleResource;
import com.csscaps.auth.model.SysUserRole;
import com.csscaps.system.utils.UUIDGenerator;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * @file  SysRoleServiceImpl.java
 * @author wanglei
 * @version 0.1
 * @todo	SysRole业务操作接口 ,基本的增删改成方法直接继承自SysRoleDao,这里不需要重写
 *        该类的方法只实现业务层的需要
 * Copyright(C),2015
 *	 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Service
public class SysRoleService extends SysRoleDao{

	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	
	@Autowired
	private SysRoleResourceDao sysRoleResourceDao;
	
	/**
	 * 删除角色信息
	 * @param roleIds
	 * @return
	 */
	public Map remove(String roleIds ){
		Map map = new HashMap();
		 //删除角色信息
		String[] roleIdArr = roleIds.split(",");
		for(int i=0;i<roleIdArr.length;i++){
			 //查询角色有没有被用户使用
			 SysUserRole userRole = new SysUserRole();
			 userRole.setUserroleRoleUuid(roleIdArr[i]);
			 List<SysUserRole> userRoleList = sysUserRoleDao.selectByVo(userRole);
			  //如果没有用户使用该角色
			 if(userRoleList.size()==0){
				   //step 1：删除角色信息
				    sysRoleDao.deleteByPk(roleIdArr[i]);
					//step 2 ：删除角色上全部的的角色菜单关系信息
				    SysRoleResource roleResource = new SysRoleResource();
				    roleResource.setRoleresourceRoleId(roleIdArr[i]);
				    sysRoleResourceDao.deleteByVo(roleResource);
					map.put("code", "200"); 
					map.put("massage", "角色删除成！"); 
					
			 }else{ //存在用户使用该角色
					 StringBuffer userAccont = new StringBuffer();
					 for(int k = 0 ;k <userRoleList.size() ;k ++){
					  userAccont.append(userRoleList.get(k).getUserroleUserAccount() +";");
					 }
					 map.put("code", "500"); 
					 map.put("massage", "[该角色被用户"+userAccont+"使用]"); 
					 break;
				  }
			}	
		return map;
	} 
	
	/**
	 * 用户角色授权 ，支持多对多关系
	 * @param userIdArr
	 * @return
	 */
	@Transactional
	public void authorize( String parameter )  throws Exception{
			 JSONObject parameterObj = JSONObject.fromObject(parameter);
			 JSONArray roleIdJSONArr = JSONArray.fromObject(parameterObj.get("roleList"));
			 JSONArray resouceIdJSONArr = JSONArray.fromObject(parameterObj.get("resouseArr"));
		     String[] roleIdArr = new String[roleIdJSONArr.size()];
			for(int i =0 ;i< roleIdJSONArr.size(); i++){
				roleIdArr[i] = String.valueOf(roleIdJSONArr.get(i));
			}
			for(int i=0;i<roleIdArr.length;i++){
				//step 1 ：删除角色上的所有资源信息
				SysRoleResource sysRoleResource =  new SysRoleResource();
				sysRoleResource.setRoleresourceRoleId(roleIdArr[i]);
				sysRoleResourceDao.deleteByVo(sysRoleResource);
				//step 2 ：构建角色资源关系
			 	  for(Object resouceId : resouceIdJSONArr) {
			           if(!exists(roleIdArr[i], String.valueOf(resouceId))) {
			        	   SysRoleResource roleResource = new SysRoleResource();
			        	   roleResource.setRoleresourceRoleId(roleIdArr[i]);
			        	   roleResource.setRoleresourceResourceId(String.valueOf(resouceId));
			        	   roleResource.setRoleresourceUuid(UUIDGenerator.genUuidStr());
			        	   sysRoleResourceDao.insertByVo(roleResource);
			           }
			        }
			}
		}
	
	/**
	 * 判断角色资源在数据库中是否存在关联关系
	 * @param roleId
	 * @param resouceId
	 * @return
	 */
	   private boolean exists(String roleId, String resouceId) {       
		     SysRoleResource roleResource = new SysRoleResource();
    	     roleResource.setRoleresourceRoleId(roleId);
    	     roleResource.setRoleresourceResourceId( resouceId);
	    	 List<SysRoleResource> resolut = sysRoleResourceDao.selectByVo(roleResource);
	         return resolut.size()!=0; 
	    }
	   
	
}
