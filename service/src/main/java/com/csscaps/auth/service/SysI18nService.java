package com.csscaps.auth.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csscaps.auth.dao.SysI18nDao;
import com.csscaps.system.constant.BaseConstant;
/**
 * @file  SysI18nService.java
 * @author wanglei
 * @version 0.1
 * @todo	SysI18nService业务操作接口 ,基本的增删改成方法直接继承自SysI18nDao,这里不需要重写
 *        该类的方法只实现业务层的需要
 * Copyright(C),2015
 *	 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Service
public class SysI18nService extends SysI18nDao{

	@Autowired
	private SysI18nDao sysI18nDao;
	
	
	public Map<String,Map> getI18nMap() {
	    Map<String,Map> baseMap = new HashMap<String, Map>();
	    List<Map<String, Object>> list;
		try {
			list = sysI18nDao.selectAll();
			for (Map<String, Object> map:list) {
				baseMap.put((String) map.get(BaseConstant.I18N_ID), map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return baseMap;
	}
}
