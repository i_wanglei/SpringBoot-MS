package com.csscaps.auth.service;
import org.springframework.stereotype.Service;

import com.csscaps.auth.dao.SysUserRoleDao;
/**
 * @file  SysUserRoleServiceImpl.java
 * @author wanglei
 * @version 0.1
 * @todo	SysUserRole业务操作接口 ,基本的增删改成方法直接继承自SysUserRoleDao,这里不需要重写
 *        该类的方法只实现业务层的需要
 * Copyright(C),2015
 *	 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Service
public class SysUserRoleService extends SysUserRoleDao{

	/*@Autowired
	private SysUserRoleDao sysUserRoleDao;*/
	 
}
