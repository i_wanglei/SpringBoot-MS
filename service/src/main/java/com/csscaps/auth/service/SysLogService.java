package com.csscaps.auth.service;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csscaps.auth.dao.SysLogDao;
import com.csscaps.auth.dao.SysUserDao;
import com.csscaps.auth.model.SysLog;
import com.csscaps.auth.model.SysUser;
import com.csscaps.system.shiro.ShiroUser;
import com.csscaps.system.utils.IPUtil;
/**
 * @file  SysLogService.java
 * @author wanglei
 * @version 0.1
 * @todo	SysLog业务操作接口 ,基本的增删改成方法直接继承自SysLogDao,这里不需要重写
 *        该类的方法只实现业务层的需要
 * Copyright(C),2015
 *	 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Service
public class SysLogService extends SysLogDao{

	@Autowired
	private SysLogDao sysLogDao;
	@Autowired
	private SysUserDao sysUserDao;
	
	public int add(HttpServletRequest request,SysLog vo) {
	    if(vo == null) return 0;
	    SysLog log = addProperty( request, vo);
	    return (int)sysLogDao.insertByVo(log);
    }
    public SysLog addProperty(HttpServletRequest request ,SysLog vo){	
    	 org.apache.shiro.subject.Subject subject = SecurityUtils.getSubject();
         String userName = String.valueOf(((ShiroUser)subject.getPrincipal()).getUserAccount());
		  SysUser user = sysUserDao.selectByUserAcount(userName);
		  vo.setLogCreateUser(user.getUserCreateUser());
		  vo.setLogIp(IPUtil.getIpAddress(request));
		  vo.setLogUserAccount(user.getUserAccount());
		  vo.setLogUserRealname(user.getUserName());
		  vo.setLogOrgId(user.getUserOrgUuid());
		  vo.setLogOrgName(user.getUserOrgName());
		  vo.setLogUserUuid(user.getUserUuid());
   	return vo;
   }
    
}
