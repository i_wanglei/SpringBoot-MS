/**
 */
package com.csscaps.auth.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.csscaps.auth.model.SysRole;
import com.csscaps.system.base.BaseDao;
import com.github.pagehelper.PageInfo;
/**
 * @file  SysRoleDao.java
 * @author wanglei
 * @version 0.1
 * @todo	SysRole数据接口实现类
 * Copyright(C), 2015
 *		  xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Repository
public class SysRoleDao extends  BaseDao<SysRole>{
	 
	 
	/**
     * 配置文件中编号：(1)
	 * 对数据库进行查询并返回一个VO数组
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */

	@SuppressWarnings({ "rawtypes" })
	public List<SysRole> selectAll() throws Exception {
		return  dao.selectList("t_sys_role.selectAll"); 
	}
	
  
	
	
	/**
	 * 配置文件中编号：(2)
	 * 搜索数据库中所有与对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List<SysRole> selectByVo(SysRole vo) {
	    if(vo == null) return null;
	    return  dao.selectList("t_sys_role.selectByVO", vo);
	}
	
	
	/**
	 * 配置文件中编号：(2)
	 * 搜索数据库中所有与对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public PageInfo<SysRole> selectPage(SysRole vo) {
	    if(vo == null) return null;
	    return  dao.selectPage("t_sys_role.selectByVO", vo);
	}
	
	
	 
	
	
    
    /**
     * 配置文件中编号：(3-3)
     * 搜索数据库中与传入的主键对应的记录
     * @param pkid 与数据库主键对应
     * @return type : RpDir 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
     */
    public SysRole selectByPk(String pkid) {
    	if(pkid ==null) return null;
    	return (SysRole)dao.selectOne("t_sys_role.selectByPk", pkid);
    }
    
	/**
	 * 配置文件中编号：(4)
	 * 向数据库中插入一个VO对象
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : RpDir 返回操作是否成功
	 * @throws Exception
	 */
	public int insertByVo(SysRole vo) {
	    if(vo == null) return 0;
	    return (int)dao.insert("t_sys_role.insertByVo",vo);
    }
	
    
	
    /**
	 * 配置文件中编号：(5-2)
	 * 更新数据库中对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录记录
	 * @return type : RpDir 返回修改操作参数
	 * @throws Exception
	 */
	public SysRole updateByVO(SysRole vo) {
		if(vo == null) return null;
		dao.update("t_sys_role.updateByVO", vo);
		return vo;
	}
	   
	
	 
	/**
	 * 配置文件中编号：(7)
	 * 删除数据库中与对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : boolean 返回删除操作是否成功
	 * @throws Exception
	 */
	public boolean deleteByVo(SysRole vo)  {
		if(vo == null) return false;
		int i = dao.delete("t_sys_role.deleteByVO", vo);
		return i==0?false:true;
	}
	
    /**
     * 配置文件中编号：(8)
	 * 删除数据库中与传入的主键对应的一条记录
	 * @param pkid 与数据库主键对应
	 * @return type : boolean 返回删除操作是否成功,如果传入参数为空则返回false,如果操作失败则抛出Exception异常
	 * @throws Exception
	 */
    public boolean deleteByPk(String pkid)  {
		if(pkid == null || "".equals(pkid)) return false;
		dao.delete("t_sys_role.deleteByPk", pkid);
		return true;
	}
    
    /**
     * 配置文件中编号：(3-3)
     * 搜索数据库中与传入的主键对应的记录
     * @param rolesName 角色名称
     * @return type : RpDir 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
     */
    public SysRole selectByName(String rolesName) {
    	if(rolesName ==null) return null;
    	return (SysRole)dao.selectOne("t_sys_role.selectByName", rolesName);
    }
    
    

    
}
