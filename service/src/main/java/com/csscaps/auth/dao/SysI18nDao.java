/**
 */
package com.csscaps.auth.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.csscaps.system.base.BaseDao;
/**
 * @file  SysI18NDao.java
 * @author wanglei
 * @version 0.1
 * @todo	SysI18NDao数据接口实现类
 * Copyright(C), 2015
 *		  xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2018-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Repository  
public class SysI18nDao extends  BaseDao<Map<String, Object>>{
	 
	 
	/**
     * 配置文件中编号：(1)
	 * 对数据库进行查询并返回一个VO数组
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */

	@SuppressWarnings({ "rawtypes" })
	public List<Map<String, Object>> selectAll() throws Exception {
		return  dao.selectList("t_sys_i18n.selectAll"); 
	}
	
  
	
	
}
