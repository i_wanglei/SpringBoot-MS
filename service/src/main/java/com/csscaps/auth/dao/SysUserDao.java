
package com.csscaps.auth.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.csscaps.auth.model.SysUser;
import com.csscaps.system.base.BaseDao;
import com.github.pagehelper.PageInfo;

@Repository  
public class SysUserDao extends BaseDao<SysUser>{
	 
	/**
     * 配置文件中编号：(1)
	 * 对数据库进行查询并返回一个VO数组
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */
	public List<SysUser> selectAll() throws Exception {
		return  dao.selectList("t_sys_user.selectAll"); 
	}
	
	/**
	 * 配置文件中编号：(2)
	 * 搜索数据库中所有与对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */
	public List<SysUser> selectByVo(SysUser vo) {
	    if(vo == null) return null;
	    return  dao.selectList("t_sys_user.selectByVO", vo);
	}
	
	/**
	 * 配置文件中编号：(2)
	 * 搜索数据库中所有与对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
	 */
	public PageInfo<SysUser> selectPage(SysUser vo) {
	    if(vo == null) return null;
	    return dao.selectPage("t_sys_user.selectByVO", vo);
	}
    
    /**
     * 配置文件中编号：(3-3)
     * 搜索数据库中与传入的主键对应的记录
     * @param acount 用户名
     * @return type : RpDir 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
     */
    public SysUser selectByUserAcount(String acount) {
    	if(acount ==null) return null;
    	return (SysUser)dao.selectOne("t_sys_user.selectByAcount", acount);
    }
    
	/**
	 * 配置文件中编号：(4)
	 * 向数据库中插入一个VO对象
	 * @param vo 对象对应到数据库中的一条记录
	 * @return type : RpDir 返回操作是否成功
	 * @throws Exception
	 */
	public int insertByVo(SysUser vo) {
	    if(vo == null) return 0;
	    return (int)dao.insert("t_sys_user.insertSelective",vo);
    }
	
    /**
	 * 配置文件中编号：(5-2)
	 * 更新数据库中对象对应的记录
	 * @param vo 对象对应到数据库中的一条记录记录
	 * @return type : RpDir 返回修改操作参数
	 * @throws Exception
	 */
	public SysUser updateByVO(SysUser vo) {
		if(vo == null) return null;
		dao.update("t_sys_user.updateByPrimaryKeySelective", vo);
		return vo;
	}
	
    /**
     * 配置文件中编号：(8)
	 * 删除数据库中与传入的主键对应的一条记录
	 * @param pkid 与数据库主键对应
	 * @return type : boolean 返回删除操作是否成功,如果传入参数为空则返回false,如果操作失败则抛出Exception异常
	 * @throws Exception
	 */
    public boolean deleteByPk(String pkid)  {
		if(pkid == null || "".equals(pkid)) return false;
		dao.delete("t_sys_user.deleteByPrimaryKey", pkid);
		return true;
	}
    
    /**
     * 查询用户及由该用户创建的子机构
     * @param userAccount
     * @return
     */
	public List<SysUser> selectSubUser(String userAccount) {
	    if(userAccount == null) return null;
	    return  dao.selectList("t_sys_user.selectSubUser", userAccount);
	}
	
	 /**
     * 配置文件中编号：(3-3)
     * 搜索数据库中与传入的主键对应的记录
     * @param pkid 与数据库主键对应
     * @return type : RpDir 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
	 * @throws Exception
     */
    public SysUser selectByPK(String userUuid) {
    	if(userUuid ==null) return null;
    	return (SysUser)dao.selectOne("t_sys_user.selectByPrimaryKey", userUuid);
    }
	
}
