package com.csscaps.auth.controller;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csscaps.auth.model.SysLog;
import com.csscaps.auth.service.SysLogService;
import com.csscaps.system.utils.ResultUtil;
import com.github.pagehelper.PageInfo;

 /**
 * @file  SysLogcontroller.java
 * @author wanglei
 * @version 0.1
 * @todo	TODO
 * Copyright(C), 2015
 *			xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Controller
@RequestMapping(value="/SysLog") 
public class SysLogController{
    private final Log log = LogFactory.getLog(SysLogController.class);
   
	
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 新增
	 * @return
	 */
	@RequestMapping(value = "add",method=RequestMethod.POST)
	@ResponseBody
	public Map  add(@Valid  @RequestBody SysLog vo){
		
		Map resoultMap = null;
		try {
			sysLogService.insertByVo(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 修改
	 * @return
	 */
	@RequestMapping(value = "edit",method=RequestMethod.POST)
	@ResponseBody
	public Map  edit(@Valid  @RequestBody SysLog vo){
		
		Map resoultMap = null;
		try {
			sysLogService.updateByVO(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "remove",method=RequestMethod.POST)
	@ResponseBody
	public Map  remove(@Valid  @RequestBody SysLog vo){
		
		Map resoultMap = null;
		try {
			sysLogService.deleteByVo(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 查询
	 * @return
	 */
	@RequestMapping(value = "getAll",method=RequestMethod.GET)
	@ResponseBody
	public Map  getAll(){
		
		Map resoultMap = null;
		try {
			List<SysLog> list =	sysLogService.selectAll();
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	/**
	 * 根据条件查询
	 * @return
	 */
	@RequestMapping(value = "get",method=RequestMethod.POST)
	@ResponseBody
	public Map  get(@Valid  @RequestBody SysLog vo){
		
		Map resoultMap = null;
		try {
			List<SysLog> list =	sysLogService.selectByVo(vo);
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 分页查询
	 * @return
	 */
	@RequestMapping(value = "getPage",method=RequestMethod.POST)
	@ResponseBody
	public Map  getPage(@Valid  @RequestBody SysLog vo){
		
		Map resoultMap = null;
		try {
			
			PageInfo<SysLog> pageInfo =	sysLogService.selectPage(vo);
			resoultMap = ResultUtil.getSuccessResult(pageInfo);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	
}
