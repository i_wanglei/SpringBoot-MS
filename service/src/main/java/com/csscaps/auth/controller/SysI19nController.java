package com.csscaps.auth.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csscaps.auth.service.SysI18nService;
import com.csscaps.system.utils.ResultUtil;

/**
 * @file SysI19nController.java
 * @author wanglei
 * @version 0.1
 * @todo TODO Copyright(C), 2015 xi'an Coordinates Software Development Co.,
 *       Ltd. History 1. Date: 2016-07-23 04:37:01 Author: wanglei Modification:
 *       this file was created 2. ...
 */
@Controller
@RequestMapping(value = "/SysI18n")
public class SysI19nController {
	private final Log log = LogFactory.getLog(SysI19nController.class);

	@Autowired
	private SysI18nService sysI18nService;

	/**
	 * 查询系统i8n资源
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "get/{language}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getUserResouse(@PathVariable("language") String language) {
		Map resoultMap = null;
		try {
			Map map = sysI18nService.getI18nMap();
			Map i18nMap = new HashMap<>();
			Iterator entries = map.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();
				Map object = (Map) entry.getValue();
				String name = String.valueOf(object.get(language));
				i18nMap.put(entry.getKey(), name);
			}

			resoultMap = ResultUtil.getSuccessResult(i18nMap);

		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		return resoultMap;
	}

}
