package com.csscaps.auth.controller;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csscaps.auth.model.SysRole;
import com.csscaps.auth.model.SysUserRole;
import com.csscaps.auth.service.SysRoleService;
import com.csscaps.auth.service.SysUserRoleService;
import com.csscaps.system.shiro.ShiroUser;
import com.csscaps.system.utils.ResultUtil;
import com.csscaps.system.utils.UUIDGenerator;
import com.github.pagehelper.PageInfo;

 /**
 * @file  SysRolecontroller.java
 * @author wanglei
 * @version 0.1
 * @todo	TODO
 * Copyright(C), 2015
 *			xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Controller
@RequestMapping(value="/SysRole") 
public class SysRoleController{
    private final Log log = LogFactory.getLog(SysRoleController.class);
   
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 新增
	 * @return
	 */
	@RequestMapping(value = "add", method=RequestMethod.POST)
	@ResponseBody
	public Map  add(@Valid  @RequestBody SysRole vo){
		
		Map resoultMap = null;
		try {
			   if (sysRoleService.selectByName(vo.getRolesName()) == null) {
					 vo.setRolesUuid(UUIDGenerator.genUuidStr());
				  	 vo.setRolesCreateTime(new Date());
				  	 Subject subject = SecurityUtils.getSubject();
			         String userName = String.valueOf(((ShiroUser)subject.getPrincipal()).getUserAccount());
	                 vo.setRolesCreateUser(userName);
					  sysRoleService.insertByVo(vo);
					  resoultMap = ResultUtil.getSuccessResult();
					  } else {
					 resoultMap = ResultUtil.getErrorResult("角色名称已经存在");
			  }
		   } catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
	   }
		
		return resoultMap;
	}
	
	
	/**
	 * 修改
	 * @return
	 */
	@RequestMapping(value = "edit", method=RequestMethod.POST)
	@ResponseBody
	public Map  edit(@Valid  @RequestBody SysRole vo){
		Map resoultMap = null;
		try {
			vo.setRolesUpdateTime(new Date());
			sysRoleService.updateByVO(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "remove/{roleIds}", method=RequestMethod.GET)
	@ResponseBody
	public Map  remove(@PathVariable("roleIds") String roleIds){
		  Map map =	sysRoleService.remove(roleIds);
		  String code = String.valueOf(map.get("code"));
		  String massage = String.valueOf(map.get("massage"));
		  Map resoultMap = null;
	     if("200".equals(code)){
			   resoultMap  = ResultUtil.getSuccessResult();
		  }else{
			  resoultMap = ResultUtil.getErrorResult(massage);
		  }
		
		return resoultMap;
	}
	
	
	/**
	 * 查询
	 * @return
	 */
	@RequestMapping(value = "getAll", method=RequestMethod.GET)
	@ResponseBody
	public Map  getAll(){
		Map resoultMap = null;
		try {
			List<SysRole> list =	sysRoleService.selectAll();
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		return resoultMap;
	}
	
	/**
	 * 根据条件查询
	 * @return
	 */
	@RequestMapping(value = "get", method=RequestMethod.POST)
	@ResponseBody
	public Map  get(@Valid  @RequestBody SysRole vo){
		
		Map resoultMap = null;
		try {
			List<SysRole> list =	sysRoleService.selectByVo(vo);
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 分页查询
	 * @return
	 */
	@RequestMapping(value = "getPage", method=RequestMethod.POST)
	@ResponseBody
	public Map  getPage(@Valid  @RequestBody SysRole vo){
		
		Map resoultMap = null;
		try {
			   /*  Subject subject = SecurityUtils.getSubject();
				 String userName = String.valueOf(((ShiroUser)subject.getPrincipal()).getUserAccount());
				 vo.setRolesCreateUser(userName);*/
			     PageInfo<SysRole> pageInfo =	sysRoleService.selectPage(vo);
			resoultMap = ResultUtil.getSuccessResult(pageInfo);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	/**
	 * 分页查询-带选择状态
	 * @return
	 */
	@RequestMapping(value = "getCheckPage", method=RequestMethod.POST)
	@ResponseBody
	public Map  getCheckPage(@Valid  @RequestBody SysRole vo){
		
		Map resoultMap = null;
		try {
			

			     Subject subject = SecurityUtils.getSubject();
				 String userName = String.valueOf(((ShiroUser)subject.getPrincipal()).getUserAccount());
				 vo.setRolesCreateUser(userName);
			     PageInfo<SysRole> pageInfo =	sysRoleService.selectPage(vo);
			      //查询所有角色信息
				 SysUserRole userRole =  new SysUserRole();
				 userRole.setUserroleUserUuid(vo.getUserId());
				 //查询用户拥有的角色
				   List<SysUserRole> list = sysUserRoleService.selectByVo(userRole);
				   HashSet h = new HashSet();
				   for (SysUserRole object : list) {
					h.add(object.getUserroleRoleUuid());
				   }
				   for(int i=0; i<pageInfo.getList().size();i++){
						//如果该角色属于用户的角色，则修改checked = true ,前段显示使用
					    if(h.contains(pageInfo.getList().get(i).getRolesUuid())){
					    	pageInfo.getList().get(i).setChecked(true);
					     };
				    }
			resoultMap = ResultUtil.getSuccessResult(pageInfo);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	
	
	/**
	 * 角色资源授权
	 * @return
	 */
	@RequestMapping(value = "authorize", method=RequestMethod.POST)
	@ResponseBody
	public Map  authorize(@RequestBody  String parameter){
		Map resoultMap = null;
		try {
			sysRoleService.authorize(parameter);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	
	
}
