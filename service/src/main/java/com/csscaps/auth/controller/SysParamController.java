package com.csscaps.auth.controller;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csscaps.auth.model.SysParam;
import com.csscaps.auth.service.SysParamService;
import com.csscaps.system.utils.ResultUtil;
import com.github.pagehelper.PageInfo;

 /**
 * @file  SysParamcontroller.java
 * @author wanglei
 * @version 0.1
 * @todo	TODO
 * Copyright(C), 2015
 *			xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
@Controller
@RequestMapping(value="/SysParam") 
public class SysParamController{
    private final Log log = LogFactory.getLog(SysParamController.class);
   
	
	@Autowired
	private SysParamService sysParamService;
	
	/**
	 * 新增
	 * @return
	 */
	@RequestMapping(value = "add", method=RequestMethod.POST)
	@ResponseBody
	public Map  add(SysParam vo){
		
		Map resoultMap = null;
		try {
			sysParamService.insertByVo(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 修改
	 * @return
	 */
	@RequestMapping(value = "edit", method=RequestMethod.POST)
	@ResponseBody
	public Map  edit(SysParam vo){
		
		Map resoultMap = null;
		try {
			sysParamService.updateByVO(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "remove", method=RequestMethod.POST)
	@ResponseBody
	public Map  remove(SysParam vo){
		
		Map resoultMap = null;
		try {
			sysParamService.deleteByVo(vo);
			resoultMap = ResultUtil.getSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 查询
	 * @return
	 */
	@RequestMapping(value = "getAll", method=RequestMethod.GET)
	@ResponseBody
	public Map  getAll(){
		
		Map resoultMap = null;
		try {
			List<SysParam> list =	sysParamService.selectAll();
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	/**
	 * 根据条件查询
	 * @return
	 */
	@RequestMapping(value = "get", method=RequestMethod.POST)
	@ResponseBody
	public Map  get(SysParam vo){
		
		Map resoultMap = null;
		try {
			List<SysParam> list =	sysParamService.selectByVo(vo);
			resoultMap = ResultUtil.getSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	/**
	 * 分页查询
	 * @return
	 */
	@RequestMapping(value = "getPage", method=RequestMethod.POST)
	@ResponseBody
	public Map  getPage(SysParam vo){
		
		Map resoultMap = null;
		try {
			
			PageInfo<SysParam> pageInfo =	sysParamService.selectPage(vo);
			resoultMap = ResultUtil.getSuccessResult(pageInfo);
		} catch (Exception e) {
			e.printStackTrace();
			resoultMap = ResultUtil.getErrorResult();
		}
		
		return resoultMap;
	}
	
	
	
}
