/**
 * com.cictec.web.mapper.SysLog.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysLog.java
 * @author wanglei
 * @version 0.1
 * @todo	SysLog数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysLog  extends BaseEntity implements java.io.Serializable{

	private java.lang.String logUuid;
	private java.lang.String logUserUuid;
	private java.lang.String logUserAccount;
	private java.lang.String logUserRealname;
	private java.lang.String logOrgName;
	private java.lang.String logOrgId;
	private java.lang.String logModule;
	private java.lang.String logFunction;
	private java.lang.String logOperation;
	private java.lang.String logDesc;
	private java.lang.String logIp;
	private java.lang.String logType;
	private java.lang.String logEnable;
	private java.util.Date logCreateTime;
	private java.util.Date logUpdateTime;
	private java.lang.String deleteStatus;
	private java.lang.String logCreateUser;
	private java.lang.String logRemark;
	
	private java.lang.String endTime;
	private java.lang.String startTime;
	public SysLog(){
	}

	public SysLog(
		java.lang.String logUuid
	){
		this.logUuid = logUuid;
	}



	public void setLogUuid(java.lang.String value) {
		this.logUuid = value;
	}	
	public java.lang.String getLogUuid() {
		return this.logUuid;
	}

	public void setLogUserUuid(java.lang.String value) {
		this.logUserUuid = value;
	}	
	public java.lang.String getLogUserUuid() {
		return this.logUserUuid;
	}

	public void setLogUserAccount(java.lang.String value) {
		this.logUserAccount = value;
	}	
	public java.lang.String getLogUserAccount() {
		return this.logUserAccount;
	}

	public void setLogUserRealname(java.lang.String value) {
		this.logUserRealname = value;
	}	
	public java.lang.String getLogUserRealname() {
		return this.logUserRealname;
	}

	public void setLogOrgName(java.lang.String value) {
		this.logOrgName = value;
	}	
	public java.lang.String getLogOrgName() {
		return this.logOrgName;
	}

	public void setLogOrgId(java.lang.String value) {
		this.logOrgId = value;
	}	
	public java.lang.String getLogOrgId() {
		return this.logOrgId;
	}

	public void setLogModule(java.lang.String value) {
		this.logModule = value;
	}	
	public java.lang.String getLogModule() {
		return this.logModule;
	}

	public void setLogFunction(java.lang.String value) {
		this.logFunction = value;
	}	
	public java.lang.String getLogFunction() {
		return this.logFunction;
	}

	public void setLogOperation(java.lang.String value) {
		this.logOperation = value;
	}	
	public java.lang.String getLogOperation() {
		return this.logOperation;
	}

	public void setLogDesc(java.lang.String value) {
		this.logDesc = value;
	}	
	public java.lang.String getLogDesc() {
		return this.logDesc;
	}

	public void setLogIp(java.lang.String value) {
		this.logIp = value;
	}	
	public java.lang.String getLogIp() {
		return this.logIp;
	}

	public void setLogType(java.lang.String value) {
		this.logType = value;
	}	
	public java.lang.String getLogType() {
		return this.logType;
	}

	public void setLogEnable(java.lang.String value) {
		this.logEnable = value;
	}	
	public java.lang.String getLogEnable() {
		return this.logEnable;
	}

	public void setLogCreateTime(java.util.Date value) {
		this.logCreateTime = value;
	}	
	public java.util.Date getLogCreateTime() {
		return this.logCreateTime;
	}

	public void setLogUpdateTime(java.util.Date value) {
		this.logUpdateTime = value;
	}	
	public java.util.Date getLogUpdateTime() {
		return this.logUpdateTime;
	}
	
	public java.lang.String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(java.lang.String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public void setLogCreateUser(java.lang.String value) {
		this.logCreateUser = value;
	}	
	public java.lang.String getLogCreateUser() {
		return this.logCreateUser;
	}

	public void setLogRemark(java.lang.String value) {
		this.logRemark = value;
	}	
	public java.lang.String getLogRemark() {
		return this.logRemark;
	}

	public java.lang.String getEndTime() {
		return endTime;
	}

	public void setEndTime(java.lang.String endTime) {
		this.endTime = endTime;
	}

	public java.lang.String getStartTime() {
		return startTime;
	}

	public void setStartTime(java.lang.String startTime) {
		this.startTime = startTime;
	}


  	



	
}
