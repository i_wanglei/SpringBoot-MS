/**
 * com.cictec.web.mapper.SysUserRole.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysUserRole.java
 * @author wanglei
 * @version 0.1
 * @todo	SysUserRole数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysUserRole  extends BaseEntity implements java.io.Serializable{

	private java.lang.String userroleUuid;
	private java.lang.String userroleUserUuid;
	private java.lang.String userroleRoleUuid;
	private java.util.Date userroleCreateTime;
	private java.util.Date userroleUpdateTime;
	private java.lang.String deleteStatus;
	private java.lang.String userroleCreateUser;
	private java.lang.String userroleRemark;
	private java.lang.String userroleUserAccount;
	public SysUserRole(){
	}

	public SysUserRole(
		java.lang.String userroleUuid
	){
		this.userroleUuid = userroleUuid;
	}



	public void setUserroleUuid(java.lang.String value) {
		this.userroleUuid = value;
	}	
	public java.lang.String getUserroleUuid() {
		return this.userroleUuid;
	}

	public void setUserroleUserUuid(java.lang.String value) {
		this.userroleUserUuid = value;
	}	
	public java.lang.String getUserroleUserUuid() {
		return this.userroleUserUuid;
	}

	public void setUserroleRoleUuid(java.lang.String value) {
		this.userroleRoleUuid = value;
	}	
	public java.lang.String getUserroleRoleUuid() {
		return this.userroleRoleUuid;
	}

	public void setUserroleCreateTime(java.util.Date value) {
		this.userroleCreateTime = value;
	}	
	public java.util.Date getUserroleCreateTime() {
		return this.userroleCreateTime;
	}

	public void setUserroleUpdateTime(java.util.Date value) {
		this.userroleUpdateTime = value;
	}	
	public java.util.Date getUserroleUpdateTime() {
		return this.userroleUpdateTime;
	}

	public java.lang.String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(java.lang.String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public void setUserroleCreateUser(java.lang.String value) {
		this.userroleCreateUser = value;
	}	
	public java.lang.String getUserroleCreateUser() {
		return this.userroleCreateUser;
	}

	public void setUserroleRemark(java.lang.String value) {
		this.userroleRemark = value;
	}	
	public java.lang.String getUserroleRemark() {
		return this.userroleRemark;
	}

	public void setUserroleUserAccount(java.lang.String value) {
		this.userroleUserAccount = value;
	}	
	public java.lang.String getUserroleUserAccount() {
		return this.userroleUserAccount;
	}


  	



	
}
