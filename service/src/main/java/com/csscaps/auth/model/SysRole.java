/**
 * com.cictec.web.mapper.SysRole.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysRole.java
 * @author wanglei
 * @version 0.1
 * @todo	SysRole数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysRole  extends BaseEntity implements java.io.Serializable{

	private java.lang.String rolesUuid;
	private java.lang.String rolesEnable;
	private java.lang.Integer rolesNumber;
	private java.lang.String rolesName;
	private java.util.Date rolesCreateTime;
	private java.lang.String rolesCreateUser;
	private java.util.Date rolesUpdateTime;
	private java.lang.String deleteStatus;
	private java.lang.String rolesRemark;
	/**
	 * 是否选中该资源-前段使用
	 */
	private boolean checked;

	
	
	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public SysRole(){
	}

	public SysRole(
		java.lang.String rolesUuid
	){
		this.rolesUuid = rolesUuid;
	}



	public void setRolesUuid(java.lang.String value) {
		this.rolesUuid = value;
	}	
	public java.lang.String getRolesUuid() {
		return this.rolesUuid;
	}

	public void setRolesEnable(java.lang.String value) {
		this.rolesEnable = value;
	}	
	public java.lang.String getRolesEnable() {
		return this.rolesEnable;
	}

	public void setRolesNumber(java.lang.Integer value) {
		this.rolesNumber = value;
	}	
	public java.lang.Integer getRolesNumber() {
		return this.rolesNumber;
	}

	public void setRolesName(java.lang.String value) {
		this.rolesName = value;
	}	
	public java.lang.String getRolesName() {
		return this.rolesName;
	}

	public void setRolesCreateTime(java.util.Date value) {
		this.rolesCreateTime = value;
	}	
	public java.util.Date getRolesCreateTime() {
		return this.rolesCreateTime;
	}

	public void setRolesCreateUser(java.lang.String value) {
		this.rolesCreateUser = value;
	}	
	public java.lang.String getRolesCreateUser() {
		return this.rolesCreateUser;
	}

	public void setRolesUpdateTime(java.util.Date value) {
		this.rolesUpdateTime = value;
	}	
	public java.util.Date getRolesUpdateTime() {
		return this.rolesUpdateTime;
	}

 

	public java.lang.String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(java.lang.String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public void setRolesRemark(java.lang.String value) {
		this.rolesRemark = value;
	}	
	public java.lang.String getRolesRemark() {
		return this.rolesRemark;
	}


  	



	
}
