/**
 * com.cictec.web.mapper.SysRoleResource.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysRoleResource.java
 * @author wanglei
 * @version 0.1
 * @todo	SysRoleResource数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysRoleResource  extends BaseEntity implements java.io.Serializable{

	private java.lang.String roleresourceUuid;
	private java.lang.String roleresourceRoleId;
	private java.lang.String roleresourceResourceParentId;
	private java.lang.String roleresourceResourceId;
	private java.util.Date roleresourceCreateTime;
	private java.util.Date roleresourceUpdateTime;
	private java.lang.String roleresourceCreateUser;
	private java.lang.String roleresourceRemark;
	private java.lang.String deleteStatus;
	private java.lang.Integer roleresourceResourceLever;
	public SysRoleResource(){
	}

	public SysRoleResource(
		java.lang.String roleresourceUuid
	){
		this.roleresourceUuid = roleresourceUuid;
	}



	public void setRoleresourceUuid(java.lang.String value) {
		this.roleresourceUuid = value;
	}	
	public java.lang.String getRoleresourceUuid() {
		return this.roleresourceUuid;
	}

	public void setRoleresourceRoleId(java.lang.String value) {
		this.roleresourceRoleId = value;
	}	
	public java.lang.String getRoleresourceRoleId() {
		return this.roleresourceRoleId;
	}

	public void setRoleresourceResourceParentId(java.lang.String value) {
		this.roleresourceResourceParentId = value;
	}	
	public java.lang.String getRoleresourceResourceParentId() {
		return this.roleresourceResourceParentId;
	}

	public void setRoleresourceResourceId(java.lang.String value) {
		this.roleresourceResourceId = value;
	}	
	public java.lang.String getRoleresourceResourceId() {
		return this.roleresourceResourceId;
	}

	public void setRoleresourceCreateTime(java.util.Date value) {
		this.roleresourceCreateTime = value;
	}	
	public java.util.Date getRoleresourceCreateTime() {
		return this.roleresourceCreateTime;
	}

	public void setRoleresourceUpdateTime(java.util.Date value) {
		this.roleresourceUpdateTime = value;
	}	
	public java.util.Date getRoleresourceUpdateTime() {
		return this.roleresourceUpdateTime;
	}

	public void setRoleresourceCreateUser(java.lang.String value) {
		this.roleresourceCreateUser = value;
	}	
	public java.lang.String getRoleresourceCreateUser() {
		return this.roleresourceCreateUser;
	}

	public void setRoleresourceRemark(java.lang.String value) {
		this.roleresourceRemark = value;
	}	
	public java.lang.String getRoleresourceRemark() {
		return this.roleresourceRemark;
	}

	public java.lang.String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(java.lang.String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public void setRoleresourceResourceLever(java.lang.Integer value) {
		this.roleresourceResourceLever = value;
	}	
	public java.lang.Integer getRoleresourceResourceLever() {
		return this.roleresourceResourceLever;
	}


  	



	
}
