/**
 * com.cictec.web.mapper.SysResource.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysResource.java
 * @author wanglei
 * @version 0.1
 * @todo	SysResource数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysResource  extends BaseEntity implements java.io.Serializable{

	private java.lang.String resourceUuid;
	private java.lang.Integer resourceLever;
	private java.lang.String resourceParentUuid;
	private java.lang.String resourceEnable;
	private java.lang.Integer resourceNumber;
	private java.lang.String resourceName;
	private java.lang.String resourceDesc;
	private java.lang.String resourceImage;
	private java.lang.String resourceUrl;
	private java.lang.String resourceTitle;
	private java.util.Date resourceCreateTime;
	private java.util.Date resourceUpdateTime;
	private java.lang.String deleteStatus;
	private java.lang.String resourceCreateUser;
	private java.lang.String resourceRemark;
	private java.lang.String language;
	public SysResource(){
	}

	public SysResource(
		java.lang.String resourceUuid
	){
		this.resourceUuid = resourceUuid;
	}



	public void setResourceUuid(java.lang.String value) {
		this.resourceUuid = value;
	}	
	public java.lang.String getResourceUuid() {
		return this.resourceUuid;
	}

	public void setResourceLever(java.lang.Integer value) {
		this.resourceLever = value;
	}	
	public java.lang.Integer getResourceLever() {
		return this.resourceLever;
	}

	public void setResourceParentUuid(java.lang.String value) {
		this.resourceParentUuid = value;
	}	
	public java.lang.String getResourceParentUuid() {
		return this.resourceParentUuid;
	}

	public void setResourceEnable(java.lang.String value) {
		this.resourceEnable = value;
	}	
	public java.lang.String getResourceEnable() {
		return this.resourceEnable;
	}

	public void setResourceNumber(java.lang.Integer value) {
		this.resourceNumber = value;
	}	
	public java.lang.Integer getResourceNumber() {
		return this.resourceNumber;
	}

	public void setResourceName(java.lang.String value) {
		this.resourceName = value;
	}	
	public java.lang.String getResourceName() {
		return this.resourceName;
	}

	public void setResourceDesc(java.lang.String value) {
		this.resourceDesc = value;
	}	
	public java.lang.String getResourceDesc() {
		return this.resourceDesc;
	}

	public void setResourceImage(java.lang.String value) {
		this.resourceImage = value;
	}	
	public java.lang.String getResourceImage() {
		return this.resourceImage;
	}

	public void setResourceUrl(java.lang.String value) {
		this.resourceUrl = value;
	}	
	public java.lang.String getResourceUrl() {
		return this.resourceUrl;
	}

	public void setResourceTitle(java.lang.String value) {
		this.resourceTitle = value;
	}	
	public java.lang.String getResourceTitle() {
		return this.resourceTitle;
	}

	public void setResourceCreateTime(java.util.Date value) {
		this.resourceCreateTime = value;
	}	
	public java.util.Date getResourceCreateTime() {
		return this.resourceCreateTime;
	}

	public void setResourceUpdateTime(java.util.Date value) {
		this.resourceUpdateTime = value;
	}	
	public java.util.Date getResourceUpdateTime() {
		return this.resourceUpdateTime;
	}

 
	public java.lang.String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(java.lang.String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public void setResourceCreateUser(java.lang.String value) {
		this.resourceCreateUser = value;
	}	
	public java.lang.String getResourceCreateUser() {
		return this.resourceCreateUser;
	}

	public void setResourceRemark(java.lang.String value) {
		this.resourceRemark = value;
	}	
	public java.lang.String getResourceRemark() {
		return this.resourceRemark;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

}
