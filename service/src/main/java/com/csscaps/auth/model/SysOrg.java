/**
 * com.cictec.web.mapper.SysOrg.java
 */
package com.csscaps.auth.model;


import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysOrg.java
 * @author wanglei
 * @version 0.1
 * @todo	SysOrg数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysOrg  extends BaseEntity implements java.io.Serializable{

	private static final long serialVersionUID = -1022255713734976942L;
	
	private String orgUuid;
	private String orgName;
	private Integer orgType;
	private String orgParentUuid;
	private Short orgEnabled;
	private String orgDesc;
	private Integer orgSortIndex;
	private String orgTreeId;
	private java.util.Date orgCreateTime;
	private java.util.Date orgUpdateTime;
	private String orgShortName;
	private String deleteStatus;
	
	private String orgCreateUser;
	private String orgRemark;
	private String organizationCode;
	
	public SysOrg(){
	}
	public SysOrg(String orgUuid, String orgName, Integer orgType, String orgParentUuid, Short orgEnabled,
			String orgDesc, Integer orgSortIndex, String orgTreeId, java.util.Date orgCreateTime, java.util.Date orgUpdateTime,
			String orgShortName, String deleteStatus, String orgCreateUser, String orgRemark,
			String organizationCode) {
		super();
		this.orgUuid = orgUuid;
		this.orgName = orgName;
		this.orgType = orgType;
		this.orgParentUuid = orgParentUuid;
		this.orgEnabled = orgEnabled;
		this.orgDesc = orgDesc;
		this.orgSortIndex = orgSortIndex;
		this.orgTreeId = orgTreeId;
		this.orgCreateTime = orgCreateTime;
		this.orgUpdateTime = orgUpdateTime;
		this.orgShortName = orgShortName;
		this.deleteStatus = deleteStatus;
		this.orgCreateUser = orgCreateUser;
		this.orgRemark = orgRemark;
		this.organizationCode = organizationCode;
	}
	
	public String getOrganizationCode() {
		return organizationCode;
	}
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}
	public String getOrgUuid() {
		return orgUuid;
	}
	public void setOrgUuid(String orgUuid) {
		this.orgUuid = orgUuid;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Integer getOrgType() {
		return orgType;
	}
	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}
	public String getOrgParentUuid() {
		return orgParentUuid;
	}
	public void setOrgParentUuid(String orgParentUuid) {
		this.orgParentUuid = orgParentUuid;
	}
	public Short getOrgEnabled() {
		return orgEnabled;
	}
	public void setOrgEnabled(Short orgEnabled) {
		this.orgEnabled = orgEnabled;
	}
	public String getOrgDesc() {
		return orgDesc;
	}
	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}
	public Integer getOrgSortIndex() {
		return orgSortIndex;
	}
	public void setOrgSortIndex(Integer orgSortIndex) {
		this.orgSortIndex = orgSortIndex;
	}
	public String getOrgTreeId() {
		return orgTreeId;
	}
	public void setOrgTreeId(String orgTreeId) {
		this.orgTreeId = orgTreeId;
	}
	public java.util.Date getOrgCreateTime() {
		return orgCreateTime;
	}
	public void setOrgCreateTime(java.util.Date date) {
		this.orgCreateTime = date;
	}
	public java.util.Date getOrgUpdateTime() {
		return orgUpdateTime;
	}
	public void setOrgUpdateTime(java.util.Date orgUpdateTime) {
		this.orgUpdateTime = orgUpdateTime;
	}
	
	public String getOrgShortName() {
		return orgShortName;
	}
	public void setOrgShortName(String orgShortName) {
		this.orgShortName = orgShortName;
	}

	public String getDeleteStatus() {
		return deleteStatus;
	}
	public void setDeleteStatus(String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}
	public String getOrgCreateUser() {
		return orgCreateUser;
	}
	public void setOrgCreateUser(String orgCreateUser) {
		this.orgCreateUser = orgCreateUser;
	}
	public String getOrgRemark() {
		return orgRemark;
	}
	public void setOrgRemark(String orgRemark) {
		this.orgRemark = orgRemark;
	}
	
    


  	



	
}
