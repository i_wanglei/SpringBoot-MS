
package com.csscaps.auth.model;

import java.util.Date;

import com.csscaps.system.base.BaseEntity;

public class SysUser extends BaseEntity implements java.io.Serializable{
	
	private static final long serialVersionUID = 6566361358586782418L;
	
    private String userUuid;

    private String userAccount;

    private String userPassword;
    
    private String userName;

    private String userDefaultRole;

    private String userMobile;

    private String userTelephone;

    private String userOrgUuid;
    
    private String userOrgName;

    private Date userCreateTime;

    private Date userUpdateTime;

    private String userRemark;

    private String userCreateUser;

    private String email;

    private Integer approveStatus;

    private String deleteStatus;

    private Integer sortNum;

    private Integer userEnable;
    
    private String approverAccount;
    
    private Date approverTime;

    private String approverComment;    
    
    private String committerAccount;

    private Date commitTime;

    private String commitComment;
    
	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserDefaultRole() {
		return userDefaultRole;
	}

	public void setUserDefaultRole(String userDefaultRole) {
		this.userDefaultRole = userDefaultRole;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserTelephone() {
		return userTelephone;
	}

	public void setUserTelephone(String userTelephone) {
		this.userTelephone = userTelephone;
	}

	public String getUserOrgUuid() {
		return userOrgUuid;
	}

	public void setUserOrgUuid(String userOrgUuid) {
		this.userOrgUuid = userOrgUuid;
	}

	public String getUserOrgName() {
		return userOrgName;
	}

	public void setUserOrgName(String userOrgName) {
		this.userOrgName = userOrgName;
	}

	public Date getUserCreateTime() {
		return userCreateTime;
	}

	public void setUserCreateTime(Date userCreateTime) {
		this.userCreateTime = userCreateTime;
	}

	public Date getUserUpdateTime() {
		return userUpdateTime;
	}

	public void setUserUpdateTime(Date userUpdateTime) {
		this.userUpdateTime = userUpdateTime;
	}

	public String getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}

	public String getUserCreateUser() {
		return userCreateUser;
	}

	public void setUserCreateUser(String userCreateUser) {
		this.userCreateUser = userCreateUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(Integer approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Integer getSortNum() {
		return sortNum;
	}

	public void setSortNum(Integer sortNum) {
		this.sortNum = sortNum;
	}

	public Integer getUserEnable() {
		return userEnable;
	}

	public void setUserEnable(Integer userEnable) {
		this.userEnable = userEnable;
	}

	public String getApproverAccount() {
		return approverAccount;
	}

	public void setApproverAccount(String approverAccount) {
		this.approverAccount = approverAccount;
	}

	public Date getApproverTime() {
		return approverTime;
	}

	public void setApproverTime(Date approverTime) {
		this.approverTime = approverTime;
	}

	public String getApproverComment() {
		return approverComment;
	}

	public void setApproverComment(String approverComment) {
		this.approverComment = approverComment;
	}

	public String getCommitterAccount() {
		return committerAccount;
	}

	public void setCommitterAccount(String committerAccount) {
		this.committerAccount = committerAccount;
	}

	public Date getCommitTime() {
		return commitTime;
	}

	public void setCommitTime(Date commitTime) {
		this.commitTime = commitTime;
	}

	public String getCommitComment() {
		return commitComment;
	}

	public void setCommitComment(String commitComment) {
		this.commitComment = commitComment;
	}
	
}
