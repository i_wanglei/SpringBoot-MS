/**
 * com.cictec.web.mapper.SysParam.java
 */
package com.csscaps.auth.model;

import com.csscaps.system.base.BaseEntity;


/**
 * @file  SysParam.java
 * @author wanglei
 * @version 0.1
 * @todo	SysParam数据值
 * Copyright(C), 2015
 *		 xi'an Coordinates Software Development Co., Ltd.
 * History
 *   	1. Date: 2016-07-23 04:37:01
 *      	Author: wanglei
 *      	Modification: this file was created
 *   	2. ...
 */
public class SysParam  extends BaseEntity implements java.io.Serializable{

	private java.lang.String paramUuid;
	private java.lang.String paramKey;
	private java.lang.String paramValue;
	private java.lang.String paramType;
	private java.util.Date paramCreatedTime;
	private java.lang.Short paramIsValid;
	public SysParam(){
	}

	public SysParam(
		java.lang.String paramUuid
	){
		this.paramUuid = paramUuid;
	}



	public void setParamUuid(java.lang.String value) {
		this.paramUuid = value;
	}	
	public java.lang.String getParamUuid() {
		return this.paramUuid;
	}

	public void setParamKey(java.lang.String value) {
		this.paramKey = value;
	}	
	public java.lang.String getParamKey() {
		return this.paramKey;
	}

	public void setParamValue(java.lang.String value) {
		this.paramValue = value;
	}	
	public java.lang.String getParamValue() {
		return this.paramValue;
	}

	public void setParamType(java.lang.String value) {
		this.paramType = value;
	}	
	public java.lang.String getParamType() {
		return this.paramType;
	}

	public void setParamCreatedTime(java.util.Date value) {
		this.paramCreatedTime = value;
	}	
	public java.util.Date getParamCreatedTime() {
		return this.paramCreatedTime;
	}

	public void setParamIsValid(java.lang.Short value) {
		this.paramIsValid = value;
	}	
	public java.lang.Short getParamIsValid() {
		return this.paramIsValid;
	}


  	



	
}
