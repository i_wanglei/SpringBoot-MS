# SpringBoot-MS

#### 项目介绍
SpringBoot-MS :基于spring boot 的微服务管理系统。


#### 软件架构
##### 软件架构说明
1. 前端使用AngularJS架构高度封装.
2. 后端使用SpringBoot微服务开发.
3. 前端使用nginx方向代理,遇到接口则请求后端服务.
4. shiro统一权限管理(redis作为session存储).
5. API管理框架,Swagger2.
6. 日志:logback
7. 持久化采样mybaits(支持连接池主备份热切)
##### 软件功能
完善登录：账号密码模式整合Spring shiro
用户管理：用户是系统操作者，该功能主要完成系统用户配置。
机构管理：配置系统组织机构，树结构展现，可随意调整上下级。
资源管理：配置系统菜单，操作权限，按钮权限标识等。
角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
动态路由：基于AngularJS实现动态路由，后端可配置化
操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
国际化：支持52种国家语言,用户只需要在数据库配置翻译后的字段即可(字段参考:https://blog.csdn.net/junjianzhang/article/details/44999469).
##### 架构模块
前端:
![](design/image/front.png)
后端:
![](design/image/service.png)
#### 安装教程
1. 安装redis,mysql/postgresql
2. 初始化design/database 下的postgres.sql 或者 mysql.sql到对应的数据库.
3. eclipse导入项目,打开application.properties修改redis配置.
```
redis.host=192.168.1.197
redis.port=6379
redis.password=
```
4. 打开mybatis.xml配置数据库.
![](design/image/mybatis.png)
5. 执行App.java 启动后端服务.
6. nginx 配置
 ```
 server {
        listen       8001;
        server_name  localhost;
        #charset koi8-r;
        #access_log  logs/host.access.log  main;
        location / {
		    #修改为前端代码地址
            root   /media/alex/4EAE8B29AE8B0923/code/private/java/SpringBoot-MS/front;
            index  index.html index.htm;
        }
	  location ^~ /service/{
			  #前端远程代理配置。
			   proxy_pass http://127.0.0.1:8080/; #修改为后端的url地址和路径。
						 proxy_redirect      default;
						 proxy_cookie_path   /inspect/ /;
						 proxy_set_header    Host    $host;
						 proxy_set_header    X-Real-IP $remote_addr;
						 proxy_set_header    X-Forwarded-Host $host;
						 proxy_set_header    X-Forwarded-Server $host;
						 proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
						 proxy_set_header    REMOTE-HOST $remote_addr;
						 break;
	    }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
```
7. 输入[ip+8001/login.html] 登录系统(默认超管:用户名:admin 密码:admin)


#### 使用说明

1. AngularJS前端路由在app.js中配置
![](design/image/front-rout.png)
2. 资源配置
  一级菜单配置名称即可;二级菜单,需要安装AngularJS路由配置
![](design/image/resource.png)
3 .按钮中的资源地址绑定js中点击按钮要执行的方法.
![](design/image/button.png)
4.国际化:按需翻译i8n表.
![](design/image/i18n.png)
4.以上是核心开发,祝您开发愉快!
#### 系统截图
###### 登录:
![](design/image/login.png)
###### 机构管理:
![](design/image/orgnazation.png)
###### 用户管理:
![](design/image/user.png)
###### 资源管理:
![](design/image/resource.png)
###### 角色管理:
![](design/image/role.png)
###### 日志管理:
![](design/image/log.png)
###### 第三方项目对接:
![](design/image/csnd.png)