         ��g        nsfer     �+��c           : 192.168.1.197
Source Server Version : 90315
Source Host           : 192.168.1.197:5432
Source Database       : SpringBoot-MS
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90315
File Encoding         : 65001

Date: 2018-09-03 16:08:25
*/


-- ----------------------------
-- Table structure for "public"."t_sys_dictionary"
-- ----------------------------
DROP TABLE "public"."t_sys_dictionary";
CREATE TABLE "public"."t_sys_dictionary" (
"id" varchar(32) NOT NULL,
"class" varchar(10) NOT NULL,
"type" varchar(20) NOT NULL,
"code" varchar(10) DEFAULT ''::character varying NOT NULL,
"value" varchar(50) DEFAULT ''::character varying NOT NULL,
"order" int4 DEFAULT 0 NOT NULL,
"remark" varchar(100),
"delete_status" varchar(10) NOT NULL,
"create_user_id" varchar(32),
"create_time" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."t_sys_dictionary" IS '数据字典';
COMMENT ON COLUMN "public"."t_sys_dictionary"."id" IS 'UUID';
COMMENT ON COLUMN "public"."t_sys_dictionary"."class" IS '大类';
COMMENT ON COLUMN "public"."t_sys_dictionary"."type" IS '子类';
COMMENT ON COLUMN "public"."t_sys_dictionary"."code" IS '编码';
COMMENT ON COLUMN "public"."t_sys_dictionary"."order" IS '排序';
COMMENT ON COLUMN "public"."t_sys_dictionary"."remark" IS '备注';
COMMENT ON COLUMN "public"."t_sys_dictionary"."delete_status" IS '状态  "0"：正常  "1" 删除';

-- ----------------------------
-- Records of t_sys_dictionary
-- ----------------------------
INSERT INTO "public"."t_sys_dictionary" VALUES ('086fbb046b584dc099c1', '10', '10', '2', '已婚', '2', null, '0', null, '2017-01-19 21:21:38.23888');
INSERT INTO "public"."t_sys_dictionary" VALUES ('0a58224f506c4a208a1b', '7', '7', '2', '近期解矫', '2', null, '0', null, '2017-01-17 23:00:07.692851');
INSERT INTO "public"."t_sys_dictionary" VALUES ('10450615a5534a34bd6a', '2', '2', '', '在线状态', '0', null, '0', null, '2017-01-17 22:27:36.705497');
INSERT INTO "public"."t_sys_dictionary" VALUES ('11da54b5d7fa48ed9225', '5', '5', '3', 'Criminal & Victim', '3', null, '0', null, '2017-01-17 22:42:00.752151');
INSERT INTO "public"."t_sys_dictionary" VALUES ('18222284faf543d3933d', '8', '8', '3', '已损坏', '3', null, '0', null, '2017-01-17 23:12:32.757682');
INSERT INTO "public"."t_sys_dictionary" VALUES ('244cec8d17c8495dbef7', '9', '9', '', '亲属关系', '0', null, '0', null, '2017-01-19 21:15:12.513278');
INSERT INTO "public"."t_sys_dictionary" VALUES ('2560f89068c941ebb397', '7', '7', '1', '矫正中', '1', null, '0', null, '2017-01-17 22:59:36.114436');
INSERT INTO "public"."t_sys_dictionary" VALUES ('25df09aca37649f6937f', '1', '1', '', '删除状态', '0', null, '0', null, '2017-01-17 22:22:28.949054');
INSERT INTO "public"."t_sys_dictionary" VALUES ('2994f941288a46498859', '7', '7', '', '矫正状态', '0', null, '0', null, '2017-01-17 22:58:52.72036');
INSERT INTO "public"."t_sys_dictionary" VALUES ('2c7a778d529a49e0a98c', '11', '11', '2', '二级告警', '2', null, '0', null, '2017-02-09 18:31:37.512521');
INSERT INTO "public"."t_sys_dictionary" VALUES ('2d3b848d7f234ae4ab7c', '5', '5', '', '监控对象类型', '0', null, '0', null, '2017-01-17 22:40:00.612197');
INSERT INTO "public"."t_sys_dictionary" VALUES ('2d774a18375142cd9204', '9', '9', '1', '父亲', '1', null, '0', null, '2017-01-19 21:15:50.002023');
INSERT INTO "public"."t_sys_dictionary" VALUES ('3023ba2602fc4062bb26', '8', '8', '2', '使用中', '2', null, '0', null, '2017-01-17 23:11:57.259162');
INSERT INTO "public"."t_sys_dictionary" VALUES ('347a7182efd24369b8ea', '8', '8', '', '设备状态', '0', null, '0', null, '2017-01-17 23:08:03.604543');
INSERT INTO "public"."t_sys_dictionary" VALUES ('365ea67416d141a3a6d0', '9', '9', '4', '女儿', '4', null, '0', null, '2017-01-19 21:17:22.531298');
INSERT INTO "public"."t_sys_dictionary" VALUES ('3b6370e78acf4d33bb4a', '13', '13', '', '性别', '0', null, '0', null, '2017-02-24 17:44:00.630669');
INSERT INTO "public"."t_sys_dictionary" VALUES ('3c4a3f05f1674702b3be', '6', '6', '', '健康状况', '0', null, '0', null, '2017-01-17 22:43:26.187977');
INSERT INTO "public"."t_sys_dictionary" VALUES ('3eb65ffe4db643048bad', '9', '9', '2', '母亲', '2', null, '0', null, '2017-01-19 21:16:15.987022');
INSERT INTO "public"."t_sys_dictionary" VALUES ('4400d6d3408746c893d1', '11', '11', '4', '四级告警', '4', null, '0', null, '2017-02-09 18:32:22.126963');
INSERT INTO "public"."t_sys_dictionary" VALUES ('4c384a28a65d4fbf95cf', '7', '7', '3', '待解矫', '3', null, '0', null, '2017-01-17 23:00:44.846071');
INSERT INTO "public"."t_sys_dictionary" VALUES ('4ca887912086466c890f', '13', '13', '1', '男', '1', null, '0', null, '2017-02-24 17:44:29.558965');
INSERT INTO "public"."t_sys_dictionary" VALUES ('4cb8fb388c9e4b8c871d', '9', '9', '6', '妻子', '6', null, '0', null, '2017-01-19 21:18:08.810368');
INSERT INTO "public"."t_sys_dictionary" VALUES ('4f5895852e144fcba67a', '6', '6', '3', 'Bad', '3', null, '0', null, '2017-01-17 22:45:01.444279');
INSERT INTO "public"."t_sys_dictionary" VALUES ('53a6b42c9139467187eb', '6', '6', '2', 'Normal', '2', null, '0', null, '2017-01-17 22:44:33.037148');
INSERT INTO "public"."t_sys_dictionary" VALUES ('542893d978654003b667', '10', '10', '3', '离异', '3', null, '0', null, '2017-01-19 21:22:02.922389');
INSERT INTO "public"."t_sys_dictionary" VALUES ('58b44b3075c440af911c', '12', '12', '3', '自动解除', '3', null, '0', null, '2017-02-17 00:41:20.850288');
INSERT INTO "public"."t_sys_dictionary" VALUES ('59a2d25b013d466a83d3', '5', '5', '1', 'Criminal', '1', null, '0', null, '2017-01-17 22:40:45.394923');
INSERT INTO "public"."t_sys_dictionary" VALUES ('5b70b09922584eb8bce8', '3', '3', '2', '基站设备', '2', null, '0', null, '2017-01-17 22:32:05.44198');
INSERT INTO "public"."t_sys_dictionary" VALUES ('63dc094f35a84170a8fa', '11', '11', '', '告警级别', '0', null, '0', null, '2017-02-09 18:30:04.456328');
INSERT INTO "public"."t_sys_dictionary" VALUES ('6522fa71c47f4d59a3d3', '5', '5', '2', 'Victim', '2', null, '0', null, '2017-01-17 22:41:28.618112');
INSERT INTO "public"."t_sys_dictionary" VALUES ('65b1a2a95b8a4801bcf5', '2', '2', '0', '离线', '1', null, '0', null, '2017-01-17 22:28:26.490081');
INSERT INTO "public"."t_sys_dictionary" VALUES ('6f93dadc1b9b494db2fc', '9', '9', '5', '丈夫', '5', null, '0', null, '2017-01-19 21:17:48.087705');
INSERT INTO "public"."t_sys_dictionary" VALUES ('703be268026a4e34a233', '9', '9', '9', '其他', '9', null, '0', null, '2017-01-19 21:19:40.493974');
INSERT INTO "public"."t_sys_dictionary" VALUES ('7738214c90c4412ebdda', '12', '12', '4', '处理中', '4', null, '0', null, '2017-02-17 00:41:42.900874');
INSERT INTO "public"."t_sys_dictionary" VALUES ('7b3755162c8443319c1d', '9', '9', '8', '阿姨', '8', null, '0', null, '2017-01-19 21:19:03.255562');
INSERT INTO "public"."t_sys_dictionary" VALUES ('7b3f35bfbf25403c9f3e', '9', '9', '3', '儿子', '3', null, '0', null, '2017-01-19 21:16:41.016292');
INSERT INTO "public"."t_sys_dictionary" VALUES ('8569604fba84459f8e1a', '4', '4', '2', '普通矫正', '2', null, '0', null, '2017-01-17 22:37:27.076154');
INSERT INTO "public"."t_sys_dictionary" VALUES ('86829a04bec042679093', '3', '3', '', '设备类型', '0', null, '0', null, '2017-01-17 22:30:52.154228');
INSERT INTO "public"."t_sys_dictionary" VALUES ('8e357229ba3b48e0b164', '4', '4', '1', '室内监视居住', '1', null, '0', null, '2017-01-17 22:36:39.759766');
INSERT INTO "public"."t_sys_dictionary" VALUES ('904b40893ce941119988', '11', '11', '3', '三级告警', '3', null, '0', null, '2017-02-09 18:31:58.422634');
INSERT INTO "public"."t_sys_dictionary" VALUES ('adb5ffa8e059427e8714', '10', '10', '', '婚姻状况', '0', null, '0', null, '2017-01-19 21:20:50.239038');
INSERT INTO "public"."t_sys_dictionary" VALUES ('ae4b4d26f59b4dcfb9bb', '7', '7', '4', '解矫中,等待法官审核', '4', null, '0', null, '2017-01-17 23:01:43.240717');
INSERT INTO "public"."t_sys_dictionary" VALUES ('b428654c4d304de082dc', '8', '8', '1', '停用', '1', null, '0', null, '2017-01-17 23:11:25.199048');
INSERT INTO "public"."t_sys_dictionary" VALUES ('bc9686ddec884172b318', '13', '13', '2', '女', '2', null, '0', null, '2017-02-24 17:45:46.325672');
INSERT INTO "public"."t_sys_dictionary" VALUES ('be1b75a1db9544478adf', '12', '12', '2', '已解除', '2', null, '0', null, '2017-02-17 00:40:45.156657');
INSERT INTO "public"."t_sys_dictionary" VALUES ('be44bf7afc3a4cca8479', '1', '1', '0', '正常', '1', null, '0', null, '2017-01-17 22:24:54.103773');
INSERT INTO "public"."t_sys_dictionary" VALUES ('c010d10f707a48e78d13', '11', '11', '1', '一级告警', '1', null, '0', null, '2017-02-09 18:31:08.386547');
INSERT INTO "public"."t_sys_dictionary" VALUES ('c3404e3f50cb4c00a323', '8', '8', '4', '报废', '4', null, '0', null, '2017-01-17 23:13:03.778095');
INSERT INTO "public"."t_sys_dictionary" VALUES ('c9bc8cb8294b453d9691', '7', '7', '6', '矫正完毕', '6', null, '0', null, '2017-01-17 23:02:48.697184');
INSERT INTO "public"."t_sys_dictionary" VALUES ('d9f22355ca0043d49a69', '6', '6', '1', 'Good', '1', null, '0', null, '2017-01-17 22:43:58.68378');
INSERT INTO "public"."t_sys_dictionary" VALUES ('dc8724aedbba457296ee', '12', '12', '1', '待处理', '1', null, '0', null, '2017-02-17 00:40:19.859486');
INSERT INTO "public"."t_sys_dictionary" VALUES ('e07c8d743cb2435793a0', '3', '3', '1', 'gps设备', '1', null, '0', null, '2017-01-17 22:31:34.900094');
INSERT INTO "public"."t_sys_dictionary" VALUES ('e0b528fde7f249a78790', '9', '9', '7', '叔叔', '7', null, '0', null, '2017-01-19 21:18:31.106659');
INSERT INTO "public"."t_sys_dictionary" VALUES ('e1298b71753543a1a7d5', '12', '12', '', '告警状态', '0', null, '0', null, '2017-02-17 00:39:08.896465');
INSERT INTO "public"."t_sys_dictionary" VALUES ('e260bba9a3fc47a18396', '10', '10', '1', '未婚', '1', null, '0', null, '2017-01-19 21:21:17.795642');
INSERT INTO "public"."t_sys_dictionary" VALUES ('eaa93b9827cc4c82865d', '2', '2', '1', '在线', '2', null, '0', null, '2017-01-17 22:29:00.003241');
INSERT INTO "public"."t_sys_dictionary" VALUES ('eff08ddf4af149bda56b', '4', '4', '', '矫正类型', '0', null, '0', null, '2017-01-17 22:36:07.551145');
INSERT INTO "public"."t_sys_dictionary" VALUES ('f624522f7b5945999e12', '7', '7', '5', '延长矫正期', '5', null, '0', null, '2017-01-17 23:02:15.65254');
INSERT INTO "public"."t_sys_dictionary" VALUES ('f98be3c69ad04af3b3e9', '1', '1', '1', '删除', '2', null, '0', null, '2017-01-17 22:25:36.226839');

-- ----------------------------
-- Table structure for "public"."t_sys_i18n"
-- ----------------------------
DROP TABLE "public"."t_sys_i18n";
CREATE TABLE "public"."t_sys_i18n" (
"i18n_uuid" varchar(50) NOT NULL,
"i18n_lever" int4,
"i18n_enable" varchar(2),
"i18n_id" varchar(50),
"i18n_name" varchar(50),
"i18n_desc" varchar(50),
"i18n_create_time" timestamp(6),
"i18n_update_time" timestamp(6),
"i18n_delete_status" varchar(10),
"i18n_create_user" varchar(20),
"i18n_update_user" varchar(20),
"i18n_remark" varchar(50),
"CHINE_NEW" varchar(500),
"CHINE_OLD" varchar(500),
"English" varchar(500),
"FRENCH" varchar(500),
"DUTCH" varchar(500),
"GERMAN" varchar(500),
"GREEK" varchar(500),
"HUNGARIAN" varchar(500),
"ITALIAN" varchar(500),
"PORTUGUESE" varchar(500),
"SPANISH" varchar(500),
"TURKISH" varchar(500),
"POLISH" varchar(500),
"CZECH" varchar(500),
"MALAY" varchar(500),
"INDONESIAN" varchar(500),
"SLOVAK" varchar(500),
"ROMANIAN" varchar(500),
"SLOVENIAN" varchar(500),
"THAI" varchar(500),
"SERBIAN" varchar(500),
"GALICIAN" varchar(500),
"VIETNAMESE" varchar(500),
"BRAZILIAN" varchar(500),
"JAPANESE" varchar(500),
"LATINESP" varchar(500),
"FARSI" varchar(500),
"CROATIAN" varchar(500),
"RUSSIAN" varchar(500),
"ARABIC" varchar(500),
"CATALAN" varchar(500),
"DANISH" varchar(500),
"FINNISH" varchar(500),
"FRENCH_CA" varchar(500),
"SWEDISH" varchar(500),
"EUSKERA" varchar(500),
"ALBANIAN" varchar(500),
"BENGALI" varchar(500),
"BULGARIAN" varchar(500),
"CAMBODIAN" varchar(500),
"ESTONIAN" varchar(500),
"HEBREW" varchar(500),
"KOREAN" varchar(500),
"LAOTIAN" varchar(500),
"LATVIAN" varchar(500),
"LITHUANIAN" varchar(500),
"MACEDONIAN" varchar(500),
"MYANMAR" varchar(500),
"UKRAINIAN" varchar(500)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of t_sys_i18n
-- ----------------------------
INSERT INTO "public"."t_sys_i18n" VALUES ('034eee9a7ae344ebb6ac', '2', null, '034eee9a7ae344ebb6ac', '修改', null, null, null, null, null, null, null, '修改', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('0ac73947328c4d79a41a', '2', null, '0ac73947328c4d79a41a', 'csdn', null, null, null, null, null, null, null, 'csdn', null, 'csdn_en', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('0b769c29846c49c88012', '2', null, '0b769c29846c49c88012', '新增', null, null, null, null, null, null, null, '新增', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('1', '2', null, 'role_rolesEnable', 'role_rolesEnable', null, null, null, null, null, null, null, '角色状态', null, 'Enable', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('10', '2', null, 'message_remove_title', 'message_remove_title', null, null, null, null, null, null, null, '数据删除', null, 'Message Remove', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('11', '2', null, 'message_remove_content', 'message_remove_content', null, null, null, null, null, null, null, '确认删除？', null, 'are you sure remove？', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('12', '2', null, 'grid_rowNumber', 'grid_rowNumber', null, null, null, null, null, null, null, '编号', null, 'Number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('13', '2', null, 'org_add_title', 'org_add_title', null, null, null, null, null, null, null, '添加机构', null, 'OrgAdd', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('14', '2', null, 'org_edit_title', 'org_edit_title', null, null, null, null, null, null, null, '修改机构', null, 'OrgEdit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('15', '2', null, 'org_orgName', 'org_orgName', null, null, null, null, null, null, null, '机构名称', null, 'OrgName', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('16', '2', null, 'user_userAccount', 'user_userAccount', null, null, null, null, null, null, null, '用户名', null, 'Account', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('17', '2', null, 'user_userOperation', 'user_userOperation', null, null, null, null, null, null, null, '操作', null, 'Operation', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('18', '2', null, 'user_email', 'user_email', null, null, null, null, null, null, null, '邮箱地址', null, 'Email', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('18a3fb8a46f14ae6a6c6', '2', null, '18a3fb8a46f14ae6a6c6', '修改', null, null, null, null, null, null, null, '修改', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('19', '2', null, 'user_userTelephone', 'user_userTelephone', null, null, null, null, null, null, null, '用户固话', null, 'Telephone', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('1e71cbfe83f14a6085f5', '2', null, '1e71cbfe83f14a6085f5', '资源权限', null, null, null, null, null, null, null, '资源权限', null, 'Auth', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('1f3a71f528ea4e75b5dc', '2', null, '1f3a71f528ea4e75b5dc', '机构管理', null, null, null, null, null, null, null, '机构管理', null, 'Orgnization', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('2', '2', null, 'role_rolesCreateTime', 'role_rolesCreateTime', null, null, null, null, null, null, null, '角色创建时间', null, 'CreateTime', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('20', '2', null, 'user_userMobile', 'user_userMobile', null, null, null, null, null, null, null, '用户手机', null, 'Mobile', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('21', '2', null, 'user_add_title', 'user_add_title', null, null, null, null, null, null, null, '添加用户', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('22', '2', null, 'user_edit_title', 'user_edit_title', null, null, null, null, null, null, null, '编辑用户', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('23', '2', null, 'user_search_title', 'user_search_title', null, null, null, null, null, null, null, '用户查询', null, 'UserSearch', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('24', '2', null, 'user_auth_title', 'user_auth_title', null, null, null, null, null, null, null, '用户授权', null, 'Auth', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('25', '2', null, 'role_add_title', 'role_add_title', null, null, null, null, null, null, null, '添加角色', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('26', '2', null, 'role_edit_title', 'role_edit_title', null, null, null, null, null, null, null, '修改角色', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('27', '2', null, 'role_search_title', 'role_search_title', null, null, null, null, null, null, null, '角色查询', null, 'Search', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('28', '2', null, 'role_auth_title', 'role_auth_title', null, null, null, null, null, null, null, '角色授权', null, 'Auth', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('29', '2', null, 'role_rolesName', 'role_rolesName', null, null, null, null, null, null, null, '角色名称', null, 'Name', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('3', '2', null, 'resource_add_title', 'resource_add_title', null, null, null, null, null, null, null, '新增资源', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('30', '2', null, 'role_rolesDesc', 'role_rolesDesc', null, null, null, null, null, null, null, '角色描述', null, 'Desc', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('31', '2', null, 'role_rolesCreateUser', 'role_rolesCreateUser', null, null, null, null, null, null, null, '创建人', null, 'CreateUser', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('32', '2', null, 'resource_resourceEnable', 'resource_resourceEnable', null, null, null, null, null, null, null, '资源状态', null, 'Enable', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('33', '2', null, 'resource_resourceLevel', 'resource_resourceLevel', null, null, null, null, null, null, null, '资源等级', null, 'Level', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('337a19949a774cce82e4', '2', null, '337a19949a774cce82e4', '系统管理', null, null, null, null, null, null, null, '系统管理', null, 'SystemManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('34', '2', null, 'resource_resourceDesc', 'resource_resourceDesc', null, null, null, null, null, null, null, '资源描述', null, 'Desc', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('35', '2', null, 'resource_resourceImage', 'resource_resourceImage', null, null, null, null, null, null, null, '资源图标', null, 'Image', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('36', '2', null, 'resource_resourceUrl', 'resource_resourceUrl', null, null, null, null, null, null, null, '资源地址', null, 'Url', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('37', '2', null, 'resource_resourceTitle', 'resource_resourceTitle', null, null, null, null, null, null, null, '资源编号', null, 'Title', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('38', '2', null, 'log_logUserAccount', 'log_logUserAccount', null, null, null, null, null, null, null, '登录用户', null, 'Account', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('39', '2', null, 'user_userName', 'user_userName', null, null, null, null, null, null, null, '用户名', null, 'userName', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('4', '2', null, 'resource_edit_title', 'resource_edit_title', null, null, null, null, null, null, null, '资源编辑', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('40', '2', null, 'user_userCreateTime', 'user_userCreateTime', null, null, null, null, null, null, null, '创建时间', null, 'CreateTime', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('41', '2', null, 'user_userEnable', 'user_userEnable', null, null, null, null, null, null, null, '用户状态', null, 'Enable', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('42', '2', null, 'user_userOrgName', 'user_userOrgName', null, null, null, null, null, null, null, '所属机构', null, 'OrgName', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('43', '2', null, 'log_startTime', 'log_startTime', null, null, null, null, null, null, null, '开始时间', null, 'SartTime', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('44', '2', null, 'log_endTime', 'log_endTime', null, null, null, null, null, null, null, '结束时间', null, 'EndTime', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('45', '2', null, 'log_logModule', 'log_logModule', null, null, null, null, null, null, null, '模块', null, 'Module', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('46', '2', null, 'log_logFunction', 'log_logFunction', null, null, null, null, null, null, null, '方法', null, 'Function', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('47', '2', null, 'log_logOperation', 'log_logOperation', null, null, null, null, null, null, null, '操作', null, 'Operation', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('47', '2', null, 'user_resetPassword', '重置密码', null, null, null, null, null, null, null, '重置密码', null, 'Reset Password', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('48', '2', null, 'log_logDesc', 'log_logDesc', null, null, null, null, null, null, null, '描述', null, 'Desc', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('48', null, null, 'message_resetPassword_title', '重置密码', null, null, null, null, null, null, null, '重置密码', null, 'Reset Password', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('49', '2', null, 'log_logIp', 'log_logIp', null, null, null, null, null, null, null, 'IP', null, 'IP', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('49', null, null, 'message_resetPassword_content', '重置密码确认消息', null, null, null, null, null, null, null, '您确认重置密码?', null, 'Are You Sure Reset Password?', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('5', '2', null, 'resource_resourceName', 'resource_resourceName', null, null, null, null, null, null, null, '资源名称', null, 'Name', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('50', '2', null, 'log_logType', 'log_logType', null, null, null, null, null, null, null, '日志类型', null, 'Type', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('50', null, null, 'status_enable', '数据可用状态', null, null, null, null, null, null, null, '启用', null, 'Enable', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('51', '2', null, 'log_logCreateTime', 'log_logCreateTime', null, null, null, null, null, null, null, '创建时间', null, 'CreateTime', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('51', null, null, 'status_unable', '数据不可用状态', null, null, null, null, null, null, null, '禁用', null, 'Unable', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('52', null, null, 'message_grid_check', '请选择一行进行操作', null, null, null, null, null, null, null, '请选择一行进行操作!', null, 'Please select a data operation.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('53', null, null, 'user_userGender', '用户性别', null, null, null, null, null, null, null, '性别', null, 'Gender', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('54', null, null, 'resource_resourceLever_button', '按钮级别', null, null, null, null, null, null, null, '按钮级别', null, 'ButtonLevel', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('55', null, null, 'resource_resourceLever_meun', '按钮级别', null, null, null, null, null, null, null, '菜单级别', null, 'Meunevel', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('599786141d11494c975d', '2', null, '599786141d11494c975d', '数据权限', null, null, null, null, null, null, null, '数据权限', null, 'Permession', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('6', '2', null, 'resource_resourceNumber', 'resource_resourceNumber', null, null, null, null, null, null, null, '资源编号', null, 'Number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('61f67a1d1df34170ad94', '2', null, '61f67a1d1df34170ad94', '新建', null, null, null, null, null, null, null, '新建', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('6b3ae0452d20423dbc31', '2', null, '6b3ae0452d20423dbc31', '日志管理', null, null, null, null, null, null, null, '日志管理', null, 'LogManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('6e454c18f43a4806b9bf', '2', null, '6e454c18f43a4806b9bf', '微服务管理系统', null, null, null, null, null, null, null, '微服务管理系统', null, 'SpringBootManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('7', '2', null, 'btn_cancel', 'btn_cancel', null, null, null, null, null, null, null, '取消', null, 'Cancel', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('8', '2', null, 'btn_ok', 'btn_ok', null, null, null, null, null, null, null, '确定', null, 'OK', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('85cc62b417424bd59b89', '2', null, '85cc62b417424bd59b89', '删除', null, null, null, null, null, null, null, '删除', null, 'Remove', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('9', '2', null, 'btn_query', 'btn_query', null, null, null, null, null, null, null, '查询', null, 'Query', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('99c24e2867ef4a089f71', '2', null, '99c24e2867ef4a089f71', '查询', null, null, null, null, null, null, null, '查询', null, 'Search', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('a3ba778b221d46878d7b', '2', null, 'a3ba778b221d46878d7b', '修改', null, null, null, null, null, null, null, '修改', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('a7aac4d4f0764dde9b42', '2', null, 'a7aac4d4f0764dde9b42', '用户管理', null, null, null, null, null, null, null, '用户管理', null, 'UserManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('c18ff924005746deb4b4', '2', null, 'c18ff924005746deb4b4', '资源管理', null, null, null, null, null, null, null, '资源管理', null, 'ResourceManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('d178db956f0a4f7e90de', '2', null, 'd178db956f0a4f7e90de', '第三方集成', null, null, null, null, null, null, null, '第三方集成', null, 'third_en', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('d8b761b41a754b4e948c', '2', null, 'd8b761b41a754b4e948c', '新建', null, null, null, null, null, null, null, '新建', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('e33eab3aadee411daf31', '2', null, 'e33eab3aadee411daf31', '删除', null, null, null, null, null, null, null, '删除', null, 'Remove', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('e4cca42f3b35406e9e7a', '2', null, 'e4cca42f3b35406e9e7a', '删除', null, null, null, null, null, null, null, '删除', null, 'Remove', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('e4e2ea4d637e4423a76c', '2', null, 'e4e2ea4d637e4423a76c', '授权', null, null, null, null, null, null, null, '授权', null, 'Auth', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('f18a09798924418a9242', '2', null, 'f18a09798924418a9242', '查询', null, null, null, null, null, null, null, '查询', null, 'Search', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('f32d7c9ea0a549e98c8d', '2', null, 'f32d7c9ea0a549e98c8d', '修改', null, null, null, null, null, null, null, '修改', null, 'Edit', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('f3a4a04821de4386bc2a', '2', null, 'f3a4a04821de4386bc2a', '新增', null, null, null, null, null, null, null, '新增', null, 'Add', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('f81f6c6e468d4195a817', '2', null, 'f81f6c6e468d4195a817', '权限管理', null, null, null, null, null, null, null, '权限管理', null, 'RoleManager', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "public"."t_sys_i18n" VALUES ('fc688b236ede4c2a87d6', '2', null, 'fc688b236ede4c2a87d6', '删除', null, null, null, null, null, null, null, '删除', null, 'Remove', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for "public"."t_sys_log"
-- ----------------------------
DROP TABLE "public"."t_sys_log";
CREATE TABLE "public"."t_sys_log" (
"log_uuid" varchar(20) NOT NULL,
"log_user_uuid" varchar(20),
"log_user_account" varchar(20),
"log_user_realname" varchar(20),
"log_org_name" varchar(50),
"log_org_id" varchar(20),
"log_module" varchar(20),
"log_function" varchar(10),
"log_operation" varchar(100),
"log_desc" varchar(20),
"log_ip" varchar(20),
"log_type" varchar(20),
"log_enable" varchar(2),
"log_create_time" timestamp(6),
"log_update_time" timestamp(6),
"delete_status" varchar(10),
"log_create_user" varchar(20),
"log_remark" varchar(50)
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."t_sys_log" IS '日志表';
COMMENT ON COLUMN "public"."t_sys_log"."log_uuid" IS '日志表uuid';
COMMENT ON COLUMN "public"."t_sys_log"."log_user_uuid" IS '用户id';
COMMENT ON COLUMN "public"."t_sys_log"."log_user_account" IS '用户登录名';
COMMENT ON COLUMN "public"."t_sys_log"."log_user_realname" IS '用户真实';
COMMENT ON COLUMN "public"."t_sys_log"."log_org_name" IS '用户所属机构';
COMMENT ON COLUMN "public"."t_sys_log"."log_org_id" IS '机构ID';
COMMENT ON COLUMN "public"."t_sys_log"."log_module" IS '模块名称';
COMMENT ON COLUMN "public"."t_sys_log"."log_function" IS '方法名称';
COMMENT ON COLUMN "public"."t_sys_log"."log_operation" IS '操作描述';
COMMENT ON COLUMN "public"."t_sys_log"."log_desc" IS '描述';
COMMENT ON COLUMN "public"."t_sys_log"."log_ip" IS 'IP地址';
COMMENT ON COLUMN "public"."t_sys_log"."log_type" IS '日志类型[异常，操作，登陆]';
COMMENT ON COLUMN "public"."t_sys_log"."log_enable" IS '是否可用';
COMMENT ON COLUMN "public"."t_sys_log"."log_create_time" IS '创建时间';
COMMENT ON COLUMN "public"."t_sys_log"."log_update_time" IS '跟新时间';
COMMENT ON COLUMN "public"."t_sys_log"."delete_status" IS '逻辑删除标识符';
COMMENT ON COLUMN "public"."t_sys_log"."log_create_user" IS '创建人';
COMMENT ON COLUMN "public"."t_sys_log"."log_remark" IS '备注';

-- ----------------------------
-- Records of t_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."t_sys_org"
-- ----------------------------
DROP TABLE "public"."t_sys_org";
CREATE TABLE "public"."t_sys_org" (
"org_uuid" varchar(32) NOT NULL,
"org_name" varchar(32),
"org_type" int4,
"org_parent_uuid" varchar(32),
"org_enabled" int2,
"org_desc" varchar(50),
"org_tree_id" varchar(32),
"org_create_time" timestamp(6),
"org_update_time" timestamp(6),
"org_short_name" varchar(30),
"org_create_user" varchar(20),
"org_remark" varchar(50),
"sort_num" int4,
"organization_code" varchar(50),
"delete_status" varchar(10)
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."t_sys_org" IS '组织机构表';
COMMENT ON COLUMN "public"."t_sys_org"."org_uuid" IS '组织机构编号';
COMMENT ON COLUMN "public"."t_sys_org"."org_name" IS '组织机构名称';
COMMENT ON COLUMN "public"."t_sys_org"."org_type" IS '组织类型';
COMMENT ON COLUMN "public"."t_sys_org"."org_parent_uuid" IS '上级机构';
COMMENT ON COLUMN "public"."t_sys_org"."org_enabled" IS '是否启用（1：启用0：禁用）';
COMMENT ON COLUMN "public"."t_sys_org"."org_desc" IS '组织机构描述';
COMMENT ON COLUMN "public"."t_sys_org"."org_tree_id" IS '树编号';
COMMENT ON COLUMN "public"."t_sys_org"."org_create_time" IS '创建时间';
COMMENT ON COLUMN "public"."t_sys_org"."org_update_time" IS '更新时间';
COMMENT ON COLUMN "public"."t_sys_org"."org_short_name" IS '简称';
COMMENT ON COLUMN "public"."t_sys_org"."org_create_user" IS '创建人';
COMMENT ON COLUMN "public"."t_sys_org"."org_remark" IS '备注';
COMMENT ON COLUMN "public"."t_sys_org"."sort_num" IS '排序编号';
COMMENT ON COLUMN "public"."t_sys_org"."organization_code" IS '组织编码: 邮政编码';
COMMENT ON COLUMN "public"."t_sys_org"."delete_status" IS '状态  "0"：正常  "1" 删除';

-- ----------------------------
-- Records of t_sys_org
-- ----------------------------
INSERT INTO "public"."t_sys_org" VALUES ('1b618330446f41d99bdc', '国家级机构', null, null, '1', null, null, '2017-02-16 00:00:00', null, null, null, null, null, '001', '0');
INSERT INTO "public"."t_sys_org" VALUES ('41f2c2417dfe47609987', '市级机构', null, '44ab2cabe37d4913b881', '1', null, null, '2018-08-30 00:00:00', null, null, null, null, null, '001001001', '0');
INSERT INTO "public"."t_sys_org" VALUES ('44ab2cabe37d4913b881', '省级机构', null, '1b618330446f41d99bdc', '1', null, null, '2017-09-22 00:00:00', null, null, null, null, null, '001001', '0');
INSERT INTO "public"."t_sys_org" VALUES ('8dc82f9114554902ad0b', '省级机构2', null, '1b618330446f41d99bdc', '1', null, null, '2017-09-22 00:00:00', null, null, null, null, null, '001002', '0');
INSERT INTO "public"."t_sys_org" VALUES ('dd63b8bbb5144d8ab55b', '市级机构2', null, '44ab2cabe37d4913b881', '1', null, null, '2018-08-30 00:00:00', null, null, null, null, null, '001001002', '0');

-- ----------------------------
-- Table structure for "public"."t_sys_param"
-- ----------------------------
DROP TABLE "public"."t_sys_param";
CREATE TABLE "public"."t_sys_param" (
"param_uuid" varchar(20) NOT NULL,
"param_key" varchar(32),
"param_value" varchar(32),
"param_type" varchar(32),
"param_created_time" timestamp(6),
"param_is_valid" int2
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."t_sys_param" IS '参数表';
COMMENT ON COLUMN "public"."t_sys_param"."param_uuid" IS '参数编号';
COMMENT ON COLUMN "public"."t_sys_param"."param_key" IS '参数名';
COMMENT ON COLUMN "public"."t_sys_param"."param_value" IS '参数值';
COMMENT ON COLUMN "public"."t_sys_param"."param_type" IS '参数类型';
COMMENT ON COLUMN "public"."t_sys_param"."param_created_time" IS '创建时间';
COMMENT ON COLUMN "public"."t_sys_param"."param_is_valid" IS '是否有效';

-- ----------------------------
-- Records of t_sys_param
-- ----------------------------
INSERT INTO "public"."t_sys_param" VALUES ('1', 'man', '男生', 'gender', null, null);
INSERT INTO "public"."t_sys_param" VALUES ('2', 'woman', '女', 'gender', null, null);
INSERT INTO "public"."t_sys_param" VALUES ('3', 'gasoline', '汽油', 'fuelType', null, null);
INSERT INTO "public"."t_sys_param" VALUES ('4', 'diesel', '柴油', 'fuelType', null, null);
INSERT INTO "public"."t_sys_param" VALUES ('5', 'electric', '电动', 'fuelType', null, null);

-- ----------------------------
-- Table structure for "public"."t_sys_resource"
-- ----------------------------
DROP TABLE "public"."t_sys_resource";
CREATE TABLE "public"."t_sys_resource" (
"resource_uuid" varchar(20) NOT NULL,
"resource_lever" int4,
"resource_parent_uuid" varchar(20),
"resource_enable" varchar(2),
"resource_number" int4,
"resource_name" varchar(50),
"resource_desc" varchar(50),
"resource_image" varchar(50),
"resource_url" varchar(500),
"resource_title" varchar(50),
"resource_create_time" timestamp(6),
"resource_update_time" timestamp(6),
"delete_status" varchar(10),
"resource_create_user" varchar(20),
"resource_remark" varchar(50)
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."t_sys_resource"."resource_number" IS '资源排序';

-- ----------------------------
-- Records of t_sys_resource
-- ----------------------------
INSERT INTO "public"."t_sys_resource" VALUES ('034eee9a7ae344ebb6ac', '3', 'a7aac4d4f0764dde9b42', '1', '11', '修改', '', 'edit.png', 'modify()', 'null', '2015-11-05 09:29:21', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('0ac73947328c4d79a41a', '2', 'd178db956f0a4f7e90de', '1', '1', 'csdn', '子系统1', 'add', 'js/modules/third/csdn/csdn.html', 'csdn', '2018-08-30 15:20:06.859', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('0b769c29846c49c88012', '3', '1f3a71f528ea4e75b5dc', '1', '11', '新增', '新增', 'add.png', 'addNew()', 'null', '2016-07-29 17:02:35.826', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('18a3fb8a46f14ae6a6c6', '3', '1f3a71f528ea4e75b5dc', '1', '11', '修改', '修改', 'edit.png', 'modify()', 'null', '2016-07-29 17:04:00.734', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('1e71cbfe83f14a6085f5', null, 'f81f6c6e468d4195a817', '1', '11', '资源权限', '', '', 'authorize()', '', '2015-11-09 14:08:36', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('1f3a71f528ea4e75b5dc', '2', '337a19949a774cce82e4', '1', '2', '机构管理', '机构管理', null, 'js/modules/auth/organization/org.html', 'orgInfo', '2016-07-29 14:45:24.726', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('337a19949a774cce82e4', '2', '6e454c18f43a4806b9bf', '1', '6', '系统管理', 'auth', 'sysManager', 'null', 'auth', '2015-11-05 09:27:51', '2016-01-12 11:10:19', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('599786141d11494c975d', '3', 'f81f6c6e468d4195a817', '1', '11', '数据权限', '', '', 'dataAuthorize()', '', '2015-11-09 15:00:35', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('61f67a1d1df34170ad94', '3', 'c18ff924005746deb4b4', '1', '11', '新建', '新建', 'add.png', 'addNew()', '1', '2015-11-09 14:15:09', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('6b3ae0452d20423dbc31', '2', '337a19949a774cce82e4', '1', '14', '日志管理', '', 'logInfo', 'js/modules/auth/log/log.html', 'logInfo', '2015-11-21 18:28:57', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('6e454c18f43a4806b9bf', '2', '', '1', '11', '微服务管理系统', '微服务管理系统', '', 'null', 'null', '2015-11-04 20:12:29', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('85cc62b417424bd59b89', '3', '1f3a71f528ea4e75b5dc', '1', '11', '删除', '删除', 'delete.png', 'removeList()', 'null', '2016-07-29 17:08:39.773', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('99c24e2867ef4a089f71', '3', 'f81f6c6e468d4195a817', '1', '11', '查询', '', 'query.png', 'showSearch()', '', '2015-11-09 14:09:18', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('a3ba778b221d46878d7b', null, 'c18ff924005746deb4b4', '1', '11', '修改', '', 'edit.png', 'modify()', '', '2015-11-09 14:15:29', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('a7aac4d4f0764dde9b42', '2', '337a19949a774cce82e4', '1', '11', '用户管理', 'user', 'user', 'js/modules/auth/user/user.html', 'userInfo', '2015-11-05 09:28:12', '2016-01-13 17:50:26', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('c18ff924005746deb4b4', '2', '337a19949a774cce82e4', '1', '13', '资源管理', 'resourceInfo', 'resourceInfo', 'js/modules/auth/resource/resourceInfo.html', 'resourceInfo', '2015-11-05 09:28:40', '2016-01-13 16:50:45', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('d178db956f0a4f7e90de', '2', '6e454c18f43a4806b9bf', '1', '9', '第三方集成', '第三方集成', null, 'null', 'third', '2018-08-30 15:17:33.494', null, null, 'admin', null);
INSERT INTO "public"."t_sys_resource" VALUES ('d8b761b41a754b4e948c', '3', 'f81f6c6e468d4195a817', '1', '11', '新建', '新建', '', 'addNew()', '', '2015-11-09 14:08:00', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('e33eab3aadee411daf31', '3', 'a7aac4d4f0764dde9b42', '1', '11', '删除', '', 'delete.png', 'removeList()', 'null', '2015-11-05 09:29:31', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('e4cca42f3b35406e9e7a', '3', 'f81f6c6e468d4195a817', '1', '11', '删除', '', 'delete.png', 'removeList()', '', '2015-11-09 14:08:58', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('e4e2ea4d637e4423a76c', '3', 'a7aac4d4f0764dde9b42', '1', '11', '授权', '', '', 'authorize()', 'null', '2015-11-05 09:30:28', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('f18a09798924418a9242', '3', 'a7aac4d4f0764dde9b42', '1', '11', '查询', '', 'query.png', 'showSearch()', 'null', '2015-11-05 09:29:48', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('f32d7c9ea0a549e98c8d', '3', 'f81f6c6e468d4195a817', '1', '11', '修改', '', 'edit.png', 'modify()', '', '2015-11-09 14:08:16', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('f3a4a04821de4386bc2a', '3', 'a7aac4d4f0764dde9b42', '1', '11', '新增', '', 'add.png', 'addNew()', 'null', '2015-11-05 09:29:09', '1900-01-01 00:00:00', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('f81f6c6e468d4195a817', '2', '337a19949a774cce82e4', '1', '12', '权限管理', 'role', 'role', 'js/modules/auth/role/role.html', 'roleInfo', '2015-11-06 13:03:42', '2016-01-22 18:11:50', '', '', '');
INSERT INTO "public"."t_sys_resource" VALUES ('fc688b236ede4c2a87d6', null, 'c18ff924005746deb4b4', '1', '11', '删除', '', 'delete.png', 'removeList()', '', '2015-11-09 14:15:49', '1900-01-01 00:00:00', '', '', '');

-- ----------------------------
-- Table structure for "public"."t_sys_role"
-- ----------------------------
DROP TABLE "public"."t_sys_role";
CREATE TABLE "public"."t_sys_role" (
"role_uuid" varchar(20) NOT NULL,
"role_enable" varchar(2),
"role_number" int4,
"role_name" varchar(50) NOT NULL,
"role_create_time" timestamp(6) NOT NULL,
"role_create_user" varchar(20),
"role_update_time" timestamp(6),
"role_remark" varchar(50),
"delete_status" varchar(10)
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."t_sys_role"."delete_status" IS '状态  "0"：正常  "1" 删除';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO "public"."t_sys_role" VALUES ('0e0d8528bd894cb8aaf9', '1', null, 'test', '2018-08-30 15:03:14.443', 'admin', null, null, null);
INSERT INTO "public"."t_sys_role" VALUES ('a71eab06bc56455f9436', '1', null, '操作员', '2018-08-30 14:21:29.676', 'admin', null, '操作员', null);
INSERT INTO "public"."t_sys_role" VALUES ('c8bab919192547fdb108', '1', null, '超级管理员', '2016-03-07 15:23:54.276', 'admin', null, '超级管理员', '0');

-- ----------------------------
-- Table structure for "public"."t_sys_role_resource"
-- ----------------------------
DROP TABLE "public"."t_sys_role_resource";
CREATE TABLE "public"."t_sys_role_resource" (
"roleresource_uuid" varchar(20) NOT NULL,
"roleresource_role_id" varchar(20),
"roleresource_resource_parent_id" varchar(20),
"roleresource_resource_id" varchar(20),
"roleresource_create_time" timestamp(6),
"roleresource_update_time" timestamp(6),
"roleresource_create_user" varchar(20),
"roleresource_remark" varchar(50),
"roleresource_resource_lever" int4,
"delete_status" varchar(10)
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."t_sys_role_resource"."delete_status" IS '状态  "0"：正常  "1" 删除';

-- ----------------------------
-- Records of t_sys_role_resource
-- ----------------------------
INSERT INTO "public"."t_sys_role_resource" VALUES ('01e4d1e3d3b64550a19b', 'a71eab06bc56455f9436', null, '337a19949a774cce82e4', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('05ccbbeb8e534769982f', 'a71eab06bc56455f9436', null, '0b769c29846c49c88012', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('08801c05965142d18ee6', '0e0d8528bd894cb8aaf9', null, '61f67a1d1df34170ad94', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('0ad8336b0f34453db0d6', 'c8bab919192547fdb108', null, 'f81f6c6e468d4195a817', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('0f03abaea6a045d09040', '0e0d8528bd894cb8aaf9', null, 'e33eab3aadee411daf31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('19a883298e5c42ef9805', 'a71eab06bc56455f9436', null, 'e33eab3aadee411daf31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('1e52350509d341c1b697', '0e0d8528bd894cb8aaf9', null, '599786141d11494c975d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('257d8b964f5f46c29c97', 'c8bab919192547fdb108', null, '034eee9a7ae344ebb6ac', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('2879e838d39a47dc8679', 'a71eab06bc56455f9436', null, '1e71cbfe83f14a6085f5', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('35f00a7c47454005a0ea', '0e0d8528bd894cb8aaf9', null, 'a7aac4d4f0764dde9b42', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('366301a2ed8e41d08207', 'c8bab919192547fdb108', null, 'e4cca42f3b35406e9e7a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('39f6f1373f2d4bb0a3ef', '0e0d8528bd894cb8aaf9', null, '99c24e2867ef4a089f71', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('3ae6937079d345048239', 'c8bab919192547fdb108', null, '1f3a71f528ea4e75b5dc', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('3c06f5276441452aa027', '0e0d8528bd894cb8aaf9', null, '6e454c18f43a4806b9bf', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('41d54de080124bee8ad1', 'a71eab06bc56455f9436', null, 'd8b761b41a754b4e948c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('46ada43ea99741a1a85a', 'a71eab06bc56455f9436', null, 'a7aac4d4f0764dde9b42', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('4aa3f25394a74d39af5f', '0e0d8528bd894cb8aaf9', null, '85cc62b417424bd59b89', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('4fe5184baf144210aa8c', '0e0d8528bd894cb8aaf9', null, '1e71cbfe83f14a6085f5', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('565e49c27d9544dd8e59', '0e0d8528bd894cb8aaf9', null, 'f81f6c6e468d4195a817', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('5d6cca5633ee4793b238', 'c8bab919192547fdb108', null, 'e4e2ea4d637e4423a76c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('5f26763470594e5e815b', '0e0d8528bd894cb8aaf9', null, 'e4e2ea4d637e4423a76c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('608971ff0ba94f3b9f2d', '0e0d8528bd894cb8aaf9', null, 'e4cca42f3b35406e9e7a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('616618f8b60b43bcb6c8', '0e0d8528bd894cb8aaf9', null, 'a3ba778b221d46878d7b', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('61711ccdebbd4f2da12b', 'c8bab919192547fdb108', null, 'f32d7c9ea0a549e98c8d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('62356badf12240f9b1dc', 'a71eab06bc56455f9436', null, 'e4e2ea4d637e4423a76c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('69b67552f33447099ca0', 'c8bab919192547fdb108', null, '0ac73947328c4d79a41a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('6d83851840f141b7986d', '0e0d8528bd894cb8aaf9', null, '1f3a71f528ea4e75b5dc', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('7063274c36234d729cb7', 'c8bab919192547fdb108', null, '6e454c18f43a4806b9bf', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('749711cf527b4786be7c', 'c8bab919192547fdb108', null, 'c18ff924005746deb4b4', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('74bfcea37dab43cda3f3', 'c8bab919192547fdb108', null, '599786141d11494c975d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('78ed22f15b054076ab81', 'a71eab06bc56455f9436', null, 'e4cca42f3b35406e9e7a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('7f15ad285b824141978d', 'c8bab919192547fdb108', null, 'f3a4a04821de4386bc2a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('80432c680bb846b6b300', 'c8bab919192547fdb108', null, '61f67a1d1df34170ad94', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('807087941b994ecaa6f0', 'c8bab919192547fdb108', null, 'f18a09798924418a9242', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('82f1e23415c84a619c40', 'a71eab06bc56455f9436', null, '034eee9a7ae344ebb6ac', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('85b6ebefa24e447e8f96', 'a71eab06bc56455f9436', null, '18a3fb8a46f14ae6a6c6', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('8cbc735b543d4e84968f', 'c8bab919192547fdb108', null, 'd8b761b41a754b4e948c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('90847987d8334c3e94ed', '0e0d8528bd894cb8aaf9', null, 'd8b761b41a754b4e948c', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('9200ac0e2f1d40b4b8ef', 'a71eab06bc56455f9436', null, 'f32d7c9ea0a549e98c8d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('958ffa4366fb40429cf1', 'c8bab919192547fdb108', null, '99c24e2867ef4a089f71', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('9821963dd3d0450e8de9', 'a71eab06bc56455f9436', null, '85cc62b417424bd59b89', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('98d87f3f71b4422193ef', '0e0d8528bd894cb8aaf9', null, '034eee9a7ae344ebb6ac', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('9b28cacb6b734544947a', 'c8bab919192547fdb108', null, 'e33eab3aadee411daf31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('a0803c40e21a49399858', 'c8bab919192547fdb108', null, '1e71cbfe83f14a6085f5', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('a9ab60bffd0c461da271', '0e0d8528bd894cb8aaf9', null, '0ac73947328c4d79a41a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('a9eeba0f7c5a41f3973f', '0e0d8528bd894cb8aaf9', null, 'f32d7c9ea0a549e98c8d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('abb5869d5ee742d0afc4', 'a71eab06bc56455f9436', null, '6b3ae0452d20423dbc31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('af0ffbd412e94f938686', '0e0d8528bd894cb8aaf9', null, '0b769c29846c49c88012', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('af6c2f7d25ff44859117', '0e0d8528bd894cb8aaf9', null, 'f3a4a04821de4386bc2a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('b11d06d0dc444c4fb5f7', '0e0d8528bd894cb8aaf9', null, 'd178db956f0a4f7e90de', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('b20b37bfa24a4c2ebbbc', 'a71eab06bc56455f9436', null, 'f81f6c6e468d4195a817', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('b2653e6d77c244779fac', '0e0d8528bd894cb8aaf9', null, 'fc688b236ede4c2a87d6', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('b499f7f3db884df2ae24', '0e0d8528bd894cb8aaf9', null, '337a19949a774cce82e4', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('b60536b8d2f94912b92c', 'c8bab919192547fdb108', null, 'a7aac4d4f0764dde9b42', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('bc493d70a56e45c7bf31', 'c8bab919192547fdb108', null, '18a3fb8a46f14ae6a6c6', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('cce0fe4ab0ba460ab2a5', 'c8bab919192547fdb108', null, '6b3ae0452d20423dbc31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('cfacc44860a741d18ff7', 'a71eab06bc56455f9436', null, '6e454c18f43a4806b9bf', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d085e245515748979e56', 'c8bab919192547fdb108', null, '85cc62b417424bd59b89', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d54d58fe9ab34df78b9e', 'c8bab919192547fdb108', null, 'd178db956f0a4f7e90de', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d6915eb0e65d46b88433', 'a71eab06bc56455f9436', null, '99c24e2867ef4a089f71', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d6e824892e274b1d9469', '0e0d8528bd894cb8aaf9', null, '6b3ae0452d20423dbc31', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d88caf684a924417bb14', 'c8bab919192547fdb108', null, '337a19949a774cce82e4', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d8b6e28495a64faab25a', 'a71eab06bc56455f9436', null, '599786141d11494c975d', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('d9ec936e0e494395a3bc', 'c8bab919192547fdb108', null, '0b769c29846c49c88012', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('e4b48ce8abd64850bf4e', 'c8bab919192547fdb108', null, 'a3ba778b221d46878d7b', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('eb36f37066c14385acc8', 'a71eab06bc56455f9436', null, '1f3a71f528ea4e75b5dc', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('ee49be2391e04977a6a2', '0e0d8528bd894cb8aaf9', null, 'f18a09798924418a9242', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('f94d53ee02fe4b268e02', 'a71eab06bc56455f9436', null, 'f3a4a04821de4386bc2a', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('f9cd90ac02f84690becf', '0e0d8528bd894cb8aaf9', null, '18a3fb8a46f14ae6a6c6', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('fbd9e92c89714872a75a', 'c8bab919192547fdb108', null, 'fc688b236ede4c2a87d6', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('fbfc506ef4e74415991e', 'a71eab06bc56455f9436', null, 'f18a09798924418a9242', null, null, null, null, null, null);
INSERT INTO "public"."t_sys_role_resource" VALUES ('ff23d503a836439fb367', '0e0d8528bd894cb8aaf9', null, 'c18ff924005746deb4b4', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for "public"."t_sys_setting"
-- ----------------------------
DROP TABLE "public"."t_sys_setting";
CREATE TABLE "public"."t_sys_setting" (
"id" varchar(32) NOT NULL,
"create_user_id" varchar(32) NOT NULL,
"create_time" timestamp(6) NOT NULL,
"delete_status" varchar(10) NOT NULL,
"full_screen" bool NOT NULL,
"play_sound" bool NOT NULL,
"filter_conditions" varchar(255) NOT NULL,
"last_longitude" varchar(32),
"last_latitude" varchar(32),
"last_zoom" int2
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."t_sys_setting" IS '系统属性设置';
COMMENT ON COLUMN "public"."t_sys_setting"."id" IS 'UUID';
COMMENT ON COLUMN "public"."t_sys_setting"."delete_status" IS '状态  "0"：正常  "1" 删除';
COMMENT ON COLUMN "public"."t_sys_setting"."full_screen" IS '是否全屏';
COMMENT ON COLUMN "public"."t_sys_setting"."play_sound" IS '是否播放声音';
COMMENT ON COLUMN "public"."t_sys_setting"."filter_conditions" IS '告警过滤 Alarmed:1,Criminal:0,Victim:0,WaitingReleased:0';
COMMENT ON COLUMN "public"."t_sys_setting"."last_longitude" IS '退出时的经度';
COMMENT ON COLUMN "public"."t_sys_setting"."last_latitude" IS '退出时的纬度';
COMMENT ON COLUMN "public"."t_sys_setting"."last_zoom" IS '退出时的缩放比例';

-- ----------------------------
-- Records of t_sys_setting
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."t_sys_user"
-- ----------------------------
DROP TABLE "public"."t_sys_user";
CREATE TABLE "public"."t_sys_user" (
"user_uuid" varchar(32) NOT NULL,
"user_account" varchar(50) NOT NULL,
"user_password" varchar(50) NOT NULL,
"user_default_role" varchar(20),
"user_mobile" varchar(20),
"user_telephone" varchar(20),
"user_org_uuid" varchar(20),
"user_create_time" timestamp(6),
"user_update_time" timestamp(6),
"user_remark" varchar(50),
"user_create_user" varchar(50),
"email" varchar(32) NOT NULL,
"approve_status" int2,
"sort_num" int4,
"user_enable" int2,
"user_name" varchar(50),
"approver_account" varchar(50),
"approver_time" timestamp(6),
"approver_comment" varchar(100),
"committer_account" varchar(50),
"commit_time" timestamp(6),
"commit_comment" varchar(100),
"organization_code" varchar(32),
"head_image" varchar(255),
"activated" bool,
"last_login_time" timestamp(6),
"locked" bool,
"delete_status" varchar(10) NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."t_sys_user"."user_account" IS '用户账号';
COMMENT ON COLUMN "public"."t_sys_user"."user_password" IS '用户密码';
COMMENT ON COLUMN "public"."t_sys_user"."user_default_role" IS '用户默认角色';
COMMENT ON COLUMN "public"."t_sys_user"."user_mobile" IS '手机号';
COMMENT ON COLUMN "public"."t_sys_user"."user_telephone" IS '座机号';
COMMENT ON COLUMN "public"."t_sys_user"."user_org_uuid" IS '用户所属机构ID';
COMMENT ON COLUMN "public"."t_sys_user"."user_create_time" IS '用户创建时间';
COMMENT ON COLUMN "public"."t_sys_user"."user_update_time" IS '用户信息更新时间';
COMMENT ON COLUMN "public"."t_sys_user"."user_remark" IS '备注';
COMMENT ON COLUMN "public"."t_sys_user"."user_create_user" IS '创建者账号';
COMMENT ON COLUMN "public"."t_sys_user"."email" IS '电子邮件';
COMMENT ON COLUMN "public"."t_sys_user"."approve_status" IS '审核状态，0：未提交审核；1：待审核；2：审核通过；3：审核不通过；';
COMMENT ON COLUMN "public"."t_sys_user"."sort_num" IS '排序编号';
COMMENT ON COLUMN "public"."t_sys_user"."user_enable" IS '账号是否可用，0：不可用；1：可用';
COMMENT ON COLUMN "public"."t_sys_user"."user_name" IS '用户昵称';
COMMENT ON COLUMN "public"."t_sys_user"."approver_account" IS '审核者';
COMMENT ON COLUMN "public"."t_sys_user"."approver_time" IS '审核时间';
COMMENT ON COLUMN "public"."t_sys_user"."approver_comment" IS '审核意见';
COMMENT ON COLUMN "public"."t_sys_user"."committer_account" IS '提交人账号';
COMMENT ON COLUMN "public"."t_sys_user"."commit_time" IS '提交时间';
COMMENT ON COLUMN "public"."t_sys_user"."commit_comment" IS '提交意见';
COMMENT ON COLUMN "public"."t_sys_user"."organization_code" IS '组织结构ID';
COMMENT ON COLUMN "public"."t_sys_user"."head_image" IS '头像';
COMMENT ON COLUMN "public"."t_sys_user"."activated" IS '是否激活';
COMMENT ON COLUMN "public"."t_sys_user"."last_login_time" IS '最后登录时间';
COMMENT ON COLUMN "public"."t_sys_user"."locked" IS '是否锁定';
COMMENT ON COLUMN "public"."t_sys_user"."delete_status" IS '状态  "0"：正常  "1" 删除';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO "public"."t_sys_user" VALUES ('21e5bdeb191147388a45', 'wl', 'MTExMTEx', null, '11', '111', '44ab2cabe37d4913b881', '2018-08-30 14:20:29.32', '2018-08-30 17:29:23.859', null, 'admin', 'iov@163.com', null, null, '1', 'wl', null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO "public"."t_sys_user" VALUES ('8358ecc3818b429abaf0', 'admin', 'YWRtaW4=', null, '11', '11', '44ab2cabe37d4913b881', '2017-03-13 15:37:39.352', '2018-08-30 14:21:03.113', null, null, '11', '0', null, '1', 'admin', null, null, null, null, null, null, null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for "public"."t_sys_user_role"
-- ----------------------------
DROP TABLE "public"."t_sys_user_role";
CREATE TABLE "public"."t_sys_user_role" (
"userrole_uuid" varchar(20) NOT NULL,
"userrole_user_uuid" varchar(20),
"userrole_role_uuid" varchar(20),
"userrole_create_time" timestamp(6),
"userrole_update_time" timestamp(6),
"delete_status" varchar(1),
"userrole_create_user" varchar(20),
"userrole_remark" varchar(50),
"userrole_user_account" varchar(50)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO "public"."t_sys_user_role" VALUES ('2d65e9a06cf546f1ae82', '21e5bdeb191147388a45', 'a71eab06bc56455f9436', '2018-08-30 14:42:42.365', null, null, null, null, 'wl');
INSERT INTO "public"."t_sys_user_role" VALUES ('68eabd931a9f4016b60c', '8358ecc3818b429abaf0', 'c8bab919192547fdb108', '2016-08-05 09:52:12.216', null, null, null, null, 'admin');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_dictionary"
-- ----------------------------
ALTER TABLE "public"."t_sys_dictionary" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_log"
-- ----------------------------
ALTER TABLE "public"."t_sys_log" ADD PRIMARY KEY ("log_uuid");

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_org"
-- ----------------------------
ALTER TABLE "public"."t_sys_org" ADD PRIMARY KEY ("org_uuid");

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_param"
-- ----------------------------
ALTER TABLE "public"."t_sys_param" ADD PRIMARY KEY ("param_uuid");

-- ----------------------------
-- Indexes structure for table t_sys_role
-- ----------------------------
CREATE UNIQUE INDEX "PK_t_em_rol1" ON "public"."t_sys_role" USING btree ("role_uuid");
ALTER TABLE "public"."t_sys_role" CLUSTER ON "PK_t_em_rol1";

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_role"
-- ----------------------------
ALTER TABLE "public"."t_sys_role" ADD PRIMARY KEY ("role_uuid");

-- ----------------------------
-- Indexes structure for table t_sys_role_resource
-- ----------------------------
CREATE UNIQUE INDEX "PK_t_em_rol2" ON "public"."t_sys_role_resource" USING btree ("roleresource_uuid");
ALTER TABLE "public"."t_sys_role_resource" CLUSTER ON "PK_t_em_rol2";

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_role_resource"
-- ----------------------------
ALTER TABLE "public"."t_sys_role_resource" ADD PRIMARY KEY ("roleresource_uuid");

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_setting"
-- ----------------------------
ALTER TABLE "public"."t_sys_setting" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table t_sys_user
-- ----------------------------
CREATE UNIQUE INDEX "PK_t_em_use1" ON "public"."t_sys_user" USING btree ("user_uuid");

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_user"
-- ----------------------------
ALTER TABLE "public"."t_sys_user" ADD PRIMARY KEY ("user_uuid");

-- ----------------------------
-- Indexes structure for table t_sys_user_role
-- ----------------------------
CREATE UNIQUE INDEX "PK_t_em_use2" ON "public"."t_sys_user_role" USING btree ("userrole_uuid");
ALTER TABLE "public"."t_sys_user_role" CLUSTER ON "PK_t_em_use2";

-- ----------------------------
-- Primary Key structure for table "public"."t_sys_user_role"
-- ----------------------------
ALTER TABLE "public"."t_sys_user_role" ADD PRIMARY KEY ("userrole_uuid");
